﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class Credits
    {
        public List<CrewResponse> crew { get; set; }

        public List<CastResponse> cast { get; set; }
    }
}
