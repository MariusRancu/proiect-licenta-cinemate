﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class ImageResponse
    {
        public List<Profile> profiles { get; set; }
    }

    public class Profile
    {
        public string file_path { get; set; }
    }
}
