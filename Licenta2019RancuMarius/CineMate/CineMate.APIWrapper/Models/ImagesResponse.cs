﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class ImagesResponse
    {
        public List<Backdrop> backdrops { get; set; }
    }

    public class Backdrop
    {
        public string file_path { get; set; }
    }
}
