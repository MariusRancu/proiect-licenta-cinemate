﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class PersonFindResult
    {
        public List<PersonResponse> results { get; set; }
    }
}
