﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class PersonListResponse
    {
        public List<PersonResult> results { get; set; }
    }

    public class PersonResult
    {
        public int id { get; set; }

        public string name { get; set; }

        public string profile_path { get; set; }
    }

    public class MovieKnownFor
    {
        public int id { get; set; }

        public string original_title { get; set; }

        public string poster_path { get; set; }

        public string release_date { get; set; }
    }
}
