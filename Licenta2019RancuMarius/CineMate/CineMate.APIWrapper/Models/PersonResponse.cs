﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class PersonResponse
    {
        public int id { get; set; }

        public string name { get; set; }

        public string profile_path { get; set; }

        public string birthday { get; set; }

        public string known_for_department { get; set; }

        public string biography { get; set; }

        public string place_of_birth { get; set; }

        public List<MovieKnownFor> known_for { get; set; }
    }
}
