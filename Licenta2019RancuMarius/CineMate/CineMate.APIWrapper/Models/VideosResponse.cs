﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.APIWrapper.Models
{
    public class VideosResponse
    {
        public List<Result> results { get; set; }
    }

    public class Result
    {
        public string key { get; set; }

        public string name { get; set; }

        public string site { get; set; }

        public string type { get; set; }
    }
}
