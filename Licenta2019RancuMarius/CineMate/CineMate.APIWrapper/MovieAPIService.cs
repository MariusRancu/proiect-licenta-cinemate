﻿using CineMate.APIWrapper.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace CineMate.APIWrapper
{
    public class MovieAPIService
    {
        private HttpClient _httpClient;

        public MovieAPIService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://api.themoviedb.org");
        }

        public async Task<MovieResponse> GetMovieByIdAsync(int id)
        {
            var movie = new MovieResponse();

            HttpResponseMessage response = await _httpClient.GetAsync($"/3/movie/{ id }?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(movie.GetType());
                    movie = ser.ReadObject(ms) as MovieResponse;
                    ms.Close();
                }
                catch
                {
                    return null;
                }
                
            }

            return movie;
        }

        public async Task<Credits> GetCreditsById(int movieId)
        {
            HttpResponseMessage response = _httpClient.GetAsync($"/3/movie/{ movieId }/credits?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488").Result;

            var credits = new Credits();
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(credits.GetType());
                credits = ser.ReadObject(ms) as Credits;

                ms.Close();
            }

            return credits;
        }

        public async Task<ImagesResponse> GetImagesPathsForMovie(int movieId)
        {
            HttpResponseMessage response = _httpClient.GetAsync($"/3/movie/{ movieId }/images?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488").Result;

            var images = new ImagesResponse();
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(images.GetType());
                images = ser.ReadObject(ms) as ImagesResponse;

                ms.Close();

                return images;
            }

            return null;
        }

        public async Task<VideosResponse> GetVideosForMovie(int movieId)
        {
            HttpResponseMessage response = _httpClient.GetAsync($"/3/movie/{ movieId }/videos?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488").Result;

            var videos = new VideosResponse();
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(videos.GetType());
                videos = ser.ReadObject(ms) as VideosResponse;

                ms.Close();

                return videos;
            }

            return null;
        }

    }

}
