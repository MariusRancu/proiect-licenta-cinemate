﻿using CineMate.APIWrapper.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace CineMate.APIWrapper
{
    public class PersonAPIService
    {
        private HttpClient _httpClient;

        public PersonAPIService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://api.themoviedb.org");
        }

        public async Task<PersonListResponse> GetPopularPersons()
        {
            var persons = new PersonListResponse();

            HttpResponseMessage response = await _httpClient.GetAsync($"/3/person/popular?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(persons.GetType());
                    persons = ser.ReadObject(ms) as PersonListResponse;
                    ms.Close();
                }
                catch
                {
                    return null;
                }

            }

            return persons;
        }

        public async Task<PersonResponse> GetPersonDetails(int id)
        {
            var person = new PersonResponse();

            HttpResponseMessage response = await _httpClient.GetAsync($"/3/person/{id}?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(person.GetType());
                    person = ser.ReadObject(ms) as PersonResponse;
                    ms.Close();
                }
                catch
                {
                    return null;
                }

            }

            return person;
        }

        public async Task<List<string>>GetPersonImages(int personId)
        {
            var imageResponse = new ImageResponse();

            HttpResponseMessage response = await _httpClient.GetAsync($"/3/person/{personId}/images?api_key=6d1d3726edc7c83d3ac9ec83b9f6e488");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(imageResponse.GetType());
                    imageResponse = ser.ReadObject(ms) as ImageResponse;
                    ms.Close();
                }
                catch
                {
                    return null;
                }
            }

            return imageResponse.profiles.Select(r => r.file_path).ToList();
        }

        public async Task<PersonFindResult> FindPerson(string fullName)
        {
            var person = new PersonFindResult();

            HttpResponseMessage response = await _httpClient.GetAsync($"/3/search/person?query={fullName}&api_key=6d1d3726edc7c83d3ac9ec83b9f6e488");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(person.GetType());
                    person = ser.ReadObject(ms) as PersonFindResult;
                    ms.Close();
                }
                catch
                {
                    return null;
                }
            }

            return person;

        }
    }
}
