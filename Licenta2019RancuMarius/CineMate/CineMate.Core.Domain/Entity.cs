﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CineMate.Domain
{
    public class Entity
    {
        [Required]
        [Key]
        public int Id { get; set; }
    }
}
