﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CineMate.Domain
{
    public class MovieDBModel : Entity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public int TMDbId { get; set; }

        public decimal Rating { get; set; }

        public int RatingsCount { get; set; }
    }
}
