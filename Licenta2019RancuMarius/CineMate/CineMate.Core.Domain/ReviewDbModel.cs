﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CineMate.Domain
{
    public class ReviewDbModel : Entity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public DateTime PostDate { get; set; }

        [Required]
        public string Text { get; set; }

        public string UserId { get; set; }

        public string Username { get; set; }

        [Required]
        public int MovieId { get; set; }
    }
}
