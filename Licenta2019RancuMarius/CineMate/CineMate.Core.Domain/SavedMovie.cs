﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain
{
    public class SavedMovie : Entity
    {
        public string UserId { get; set; }

        public int MovieId { get; set; }

        public MovieDBModel Movie { get; set; }
    }
}
