﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class MovieDetails
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int Year { get; set; }

        public decimal Rating { get; set; }

        public int RatingsCount { get; set; }

        public decimal? UserRating { get; set; }

        public string BackgroundImgPath { get; set; }

        public string PosterPath { get; set; }

        public string Description { get; set; }

        public List<Cast> Cast { get; set; }

        public List<Crew> Crew { get; set; }

        public List<string> Genres { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int RunTime { get; set; }

        public List<string> Keywords { get; set; }

        public List<AsideDetail> AsideDetails { get; set; }

        public List<Review> Reviews { get; set; }

        public List<string> ImagesPaths { get; set; } = new List<string>();

        public List<Video> VideosPaths { get; set; } = new List<Video>();
    }

    public enum AsideDetailItemType
    {
        LIST,
        STRING,
        KEYWORD
    }

    public class Video
    {
        public string Key { get; set; }

        public string Name { get; set; }

        public string Site { get; set; }

        public string Type { get; set; }
    }

    public class Cast
    {
        public string CreditId { get; set; }

        public string CastId { get; set; }
        
        public string Character { get; set; }

        public int Gender { get; set; }

        public int Id { get; set; }

        public int Order { get; set; }

        public string Name { get; set; }

        public string ProfilePath { get; set; }
    }

    public class Crew
    {
        public string CreditId { get; set; }

        public string Department { get; set; }

        public int? Gender { get; set; }

        public int Id { get; set; }

        public string Job { get; set; }

        public string Name { get; set; }

        public string ProfilePath { get; set; }
    }

    public class AsideDetail
    {
        public string EntityName { get; set; }

        public List<string> Data { get; set; }

        public AsideDetailItemType Type { get; set; }
    }
}
