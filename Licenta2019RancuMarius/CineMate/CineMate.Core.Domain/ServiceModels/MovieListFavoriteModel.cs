﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class MovieListFavoriteModel : MovieListModel
    {
        public Review UserReview { get; set; }

        public decimal? UserRating { get; set; }
    }
}
