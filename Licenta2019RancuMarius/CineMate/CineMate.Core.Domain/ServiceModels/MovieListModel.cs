﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class MovieListModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public decimal Rating { get; set; }

        public string PosterPath { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int Runtime { get; set; }

        public string TagLine { get; set; }

        public List<string> Directors { get; set; }

        public List<string> Genres { get; set; }

    }
}
