﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class PersonListModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string PosterPath { get; set; }

        public string Description { get; set; }

        public string Biography { get; set; }

        public string Job { get; set; }

        public string PlaceOfBirth { get; set; }
    }
}
