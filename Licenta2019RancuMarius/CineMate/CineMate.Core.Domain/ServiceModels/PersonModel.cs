﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class PersonModel : PersonListModel
    {
        public List<string> Images { get; set; } = new List<string>();

        public List<MovieForPerson> Filmography { get; set; } = new List<MovieForPerson>();

        public class MovieForPerson
        {
            public int Id { get; set; }

            public string MovieName { get; set; }

            public string CharacterName { get; set; }

            public int Year { get; set; }

            public string CharacterImg { get; set; }
        }
    }
}
