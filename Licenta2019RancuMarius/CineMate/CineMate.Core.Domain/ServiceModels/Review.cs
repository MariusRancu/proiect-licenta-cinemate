﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.Domain.ServiceModels
{
    public class Review
    {
        public string Title { get; set; }

        public string User { get; set; }

        public string UserId { get; set; }

        public string Description { get; set; }

        public int MovieId { get; set; }
    }
}
