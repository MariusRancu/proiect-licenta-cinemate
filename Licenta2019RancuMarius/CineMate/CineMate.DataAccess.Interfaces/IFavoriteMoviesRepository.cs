﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface IFavoriteMoviesRepository
    {
        IEnumerable<FavoriteMovie> GetFavoriteMoviesForUser(string userId);

        void AddMovieToFavorites(string userId, MovieDBModel movie);

        void DeleteFavoriteMovie(FavoriteMovie movie);

        FavoriteMovie GetFavoriteMovie(string userId, MovieDBModel movie);
    }
}
