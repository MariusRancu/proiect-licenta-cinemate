﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface IMovieRepository
    {
        void AddMovie(MovieDBModel movie);

        void AddMovies(List<MovieDBModel> movies);

        IQueryable<MovieDBModel> GetAllMovies();

        MovieDBModel GetMovieById(int id);

        void SetRatingFields(int movieId, decimal averageRating, int ratingsCount);
    }
}
