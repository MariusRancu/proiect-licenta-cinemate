﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface IRatingRepository
    {
        void AddUserRating(UserRating userRating);

        void AddUsersRatings(List<UserRating> userRatings);

        UserRating GetUserRatingForMovie(string userId, int movieId);

        IEnumerable<UserRating> GetUserRatingsForMovieId(int movieId);

        IEnumerable<UserRating> GetAllUsersRatings();


        void UpdateUserRating(int ratingIdToUpdate, UserRating userRating);
    }
}
