﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface IReviewRepository
    {
        void AddReview(ReviewDbModel review);

        IEnumerable<ReviewDbModel> GetReviewsForMovie(int movieId);
    }
}
