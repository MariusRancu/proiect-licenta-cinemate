﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface ISavedMoviesRepository
    {
        IEnumerable<SavedMovie> GetSavedMoviesForUser(string userId);

        void AddMovieToSaved(string userId, MovieDBModel movie);

        void DeleteSavedMovie(SavedMovie movie);

        SavedMovie GetSavedMovie(string userId, MovieDBModel movie);
    }
}
