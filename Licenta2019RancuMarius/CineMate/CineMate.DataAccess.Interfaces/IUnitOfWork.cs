﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CineMate.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
