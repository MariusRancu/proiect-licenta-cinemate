﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using CineMate.Domain;
using CineMate.Services.DataTools;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CineMate.DataAccess.Identity
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<MovieDBModel> Movies { get; set; }

        public DbSet<ReviewDbModel> MovieReviews { get; set; }
        
        public DbSet<UserRating> UsersRatings { get; set; }

        public DbSet<FavoriteMovie> FavoriteMovies { get; set; }

        public DbSet<SavedMovie> SavedMovies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public void SetIdentityInsertOn(List<MovieDBModel> movies)
        {
            Database.OpenConnection();
            for (int i = 0; i < movies.Count; i++)
            {
                Database.ExecuteSqlCommand("SET IDENTITY_INSERT Movies ON");
                Movies.Add(movies[i]);
                if ((i % 1000) == 0)
                {
                    SaveChanges();
                }
            }
            SaveChanges();
            Database.ExecuteSqlCommand("SET IDENTITY_INSERT Movies OFF");
            Database.CloseConnection();
        }
    }
}
