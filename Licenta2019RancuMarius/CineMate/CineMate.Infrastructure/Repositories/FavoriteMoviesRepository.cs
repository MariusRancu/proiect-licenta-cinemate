﻿using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CineMate.DataAccess.Repositories
{
    public class FavoriteMoviesRepository : IFavoriteMoviesRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public FavoriteMoviesRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddMovieToFavorites(string userId, MovieDBModel movie)
        {
            _dbContext.FavoriteMovies.Add(new FavoriteMovie() 
            {
                UserId = userId,
                Movie = movie
            });
        }

        public void DeleteFavoriteMovie(FavoriteMovie movie)
        {
            _dbContext.FavoriteMovies.Remove(movie);
        }

        public FavoriteMovie GetFavoriteMovie(string userId, MovieDBModel movie)
        {
            return _dbContext.FavoriteMovies.First(sm => sm.UserId == userId && sm.MovieId == movie.Id);
        }

        public IEnumerable<FavoriteMovie> GetFavoriteMoviesForUser(string userId)
        {
            return _dbContext.FavoriteMovies.Where(m => m.UserId == userId).Include(sm => sm.Movie);
        }
    }
}
