﻿using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CineMate.DataAccess.Repositories
{
    public class MovieCachedRepositoryDecorator : IMovieRepository
    {
        private IMovieRepository _movieRepo;
        private readonly IMemoryCache _memCache;
        private const string MOVIE_CACHE_KEY = "MovieCacheKey";
        private MemoryCacheEntryOptions cacheOptions;

        public MovieCachedRepositoryDecorator(MovieRepository movieRepo, IMemoryCache memCache)
        {
            _movieRepo = movieRepo;
            _memCache = memCache;
            cacheOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(relative: TimeSpan.FromHours(3));
        }

        public void AddMovie(MovieDBModel movie)
        {
            _movieRepo.AddMovie(movie);
        }

        public void AddMovies(List<MovieDBModel> movies)
        {
            _movieRepo.AddMovies(movies);
        }

        public IQueryable<MovieDBModel> GetAllMovies()
        {
            return _memCache.GetOrCreate(MOVIE_CACHE_KEY, entry =>
            {
                entry.SetOptions(cacheOptions);
                return _movieRepo.GetAllMovies();
            });
        }

        public MovieDBModel GetMovieById(int id)
        {
            return _movieRepo.GetMovieById(id);
        }

        public void SetRatingFields(int movieId, decimal averageRating, int ratingsCount)
        {
            _movieRepo.SetRatingFields(movieId, averageRating, ratingsCount);
        }
    }
}
