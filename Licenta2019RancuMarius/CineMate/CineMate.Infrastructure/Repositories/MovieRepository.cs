﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;

namespace CineMate.DataAccess.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        protected readonly ApplicationDbContext _dbContext;

        public MovieRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddMovie(MovieDBModel movie)
        {
            _dbContext.Movies.Add(movie);
        }

        public void AddMovies(List<MovieDBModel> movies)
        {
            _dbContext.SetIdentityInsertOn(movies);
        }

        public IQueryable<MovieDBModel> GetAllMovies()
        {
            return _dbContext.Movies;
        }

        public MovieDBModel GetMovieById(int id)
        {
            return _dbContext.Movies.First(m => m.Id == id);
        }

        public void SetRatingFields(int movieId, decimal averageRating, int ratingsCount)
        {
            MovieDBModel movieToUpdate = _dbContext.Movies.Find(movieId);
            movieToUpdate.Rating = averageRating;
            movieToUpdate.RatingsCount = ratingsCount;

        }
    }
}
