﻿using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CineMate.DataAccess.Repositories
{
    public class RatingCachedRepositoryDecorator : IRatingRepository
    {
        private IRatingRepository _ratingRepo;
        private readonly IMemoryCache _cache;
        private const string RATING_CACHE_KEY = "RatingCacheKey";
        private MemoryCacheEntryOptions cacheOptions;

        public RatingCachedRepositoryDecorator(RatingRepository ratingRepo, IMemoryCache cache)
        { 
            _ratingRepo = ratingRepo;
            _cache = cache;
            cacheOptions = new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(relative: TimeSpan.FromHours(3));
        }

        public IEnumerable<UserRating> GetAllUsersRatings()
        {
            return _cache.GetOrCreate(RATING_CACHE_KEY, entry => 
            {
                entry.SetOptions(cacheOptions);
                return _ratingRepo.GetAllUsersRatings().ToList();
            });
        }

        public void AddUserRating(UserRating userRating)
        {
            _ratingRepo.AddUserRating(userRating);
        }

        public void AddUsersRatings(List<UserRating> userRatings)
        {
            _ratingRepo.AddUsersRatings(userRatings);
        }

        public UserRating GetUserRatingForMovie(string userId, int movieId)
        {
           return _ratingRepo.GetUserRatingForMovie(userId, movieId);
        }

        public IEnumerable<UserRating> GetUserRatingsForMovieId(int movieId)
        {
            return _ratingRepo.GetUserRatingsForMovieId(movieId);
        }

        public void UpdateUserRating(int ratingIdToUpdate, UserRating userRating)
        {
            _ratingRepo.UpdateUserRating(ratingIdToUpdate, userRating);
        }
    }
}
