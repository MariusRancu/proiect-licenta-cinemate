﻿using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using System.Collections.Generic;
using System.Linq;

namespace CineMate.DataAccess.Repositories
{
    public class RatingRepository : IRatingRepository
    {
        protected readonly ApplicationDbContext _dbContext;

        public RatingRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddUserRating(UserRating userRating)
        {
            _dbContext.UsersRatings.Add(userRating);
        }

        public void AddUsersRatings(List<UserRating> userRatings)
        {
            for (int i = 0; i < userRatings.Count; i++)
            {
                _dbContext.UsersRatings.Add(userRatings[i]);

                if((i % 1000) == 0) _dbContext.SaveChanges();
            }

            _dbContext.SaveChanges();
        }

        public IEnumerable<UserRating> GetAllUsersRatings()
        {
           return _dbContext.UsersRatings;
        }

        public UserRating GetUserRatingForMovie(string userId, int movieId)
        {
           return _dbContext.UsersRatings.FirstOrDefault(ur => (ur.UserId == userId && ur.MovieId == movieId));
        }

        public IEnumerable<UserRating> GetUserRatingsForMovieId(int movieId)
        {
            if(movieId <= 0) return new List<UserRating>();

            return _dbContext.UsersRatings.Where(r => r.MovieId == movieId);
        }

        public void UpdateUserRating(int ratingIdToUpdate, UserRating userRating)
        {
            var ratingToUpdate = _dbContext.UsersRatings.Find(ratingIdToUpdate);

            ratingToUpdate = userRating;
        }
    }
}
