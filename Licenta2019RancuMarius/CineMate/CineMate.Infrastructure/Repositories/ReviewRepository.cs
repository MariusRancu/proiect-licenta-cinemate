﻿using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using System.Collections.Generic;
using System.Linq;

namespace CineMate.DataAccess.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        readonly ApplicationDbContext _dbContext;
        public ReviewRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddReview(ReviewDbModel review)
        {
            _dbContext.MovieReviews.Add(review);
        }

        public IEnumerable<ReviewDbModel> GetReviewsForMovie(int movieId)
        {
           return _dbContext.MovieReviews.Where( mr => mr.MovieId == movieId);
        }
    }
}
