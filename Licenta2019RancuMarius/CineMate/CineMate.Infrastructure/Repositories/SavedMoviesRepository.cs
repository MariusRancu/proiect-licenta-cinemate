﻿using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CineMate.DataAccess.Repositories
{
    public class SavedMoviesRepository : ISavedMoviesRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SavedMoviesRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddMovieToSaved(string userId, MovieDBModel movie)
        {
            _dbContext.SavedMovies.Add(new SavedMovie()
            {
                UserId = userId,
                Movie = movie
            });
        }

        public IEnumerable<SavedMovie> GetSavedMoviesForUser(string userId)
        {
            return _dbContext.SavedMovies.Where(m => m.UserId == userId).Include(sm => sm.Movie);
        }

        public void DeleteSavedMovie(SavedMovie movie)
        {
            _dbContext.SavedMovies.Remove(movie);
        }

        public SavedMovie GetSavedMovie(string userId, MovieDBModel movie)
        {
            return _dbContext.SavedMovies.First(sm => sm.UserId == userId && sm.MovieId == movie.Id);
        }
    }
}
