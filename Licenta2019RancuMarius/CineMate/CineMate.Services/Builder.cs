﻿using CineMate.APIWrapper.Models;
using CineMate.Domain;
using CineMate.Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CineMate.Services
{
    public static class Builder
    {
        public static MovieListModel BuildMovieListModel(MovieResponse movieResponse, MovieDBModel movieDbModel, Credits credits)
        {
            var movieListModel = new MovieListModel();

            movieListModel.Id = movieDbModel.Id;
            movieListModel.Title = movieResponse.title;
            movieListModel.Rating = movieDbModel.Rating;
            movieListModel.PosterPath = movieResponse.poster_path;
            movieListModel.Runtime = movieResponse.runtime;
            movieListModel.TagLine = movieResponse.tagline;
            movieListModel.Genres = movieResponse.genres.Select(g => g.name).ToList();
            try
            {
                movieListModel.ReleaseDate = DateTime.ParseExact(movieResponse.release_date, "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch { }
            movieListModel.Directors = GetDirectorsFromCredits(credits).Select(c => c.name).ToList();

            return movieListModel;
        }

        public static MovieListFavoriteModel BuildFavoriteMovieListModel(MovieResponse movieResponse, 
            MovieDBModel movieDbModel, Credits credits,
            ReviewDbModel userReview, decimal? userRating)
        {
            var movieListModel = new MovieListFavoriteModel();

            movieListModel.Id = movieDbModel.Id;
            movieListModel.Title = movieResponse.title;
            movieListModel.Rating = movieDbModel.Rating;
            movieListModel.PosterPath = "http://image.tmdb.org/t/p/w185/" + movieResponse.poster_path;
            movieListModel.Runtime = movieResponse.runtime;
            movieListModel.TagLine = movieResponse.tagline;
            movieListModel.Genres = movieResponse.genres.Select(g => g.name).ToList();
            try
            {
                movieListModel.ReleaseDate = DateTime.ParseExact(movieResponse.release_date, "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch { }
            movieListModel.Directors = GetDirectorsFromCredits(credits).Select(c => c.name).ToList();
            
            if(userReview != null)
            {
                movieListModel.UserReview = new Review()
                {
                    Description = userReview.Text,
                    Title = userReview.Title,
                    UserId = userReview.UserId,
                    User = userReview.Username
                };
            }
            
            movieListModel.UserRating = userRating;

            return movieListModel;
        }

        public static MovieDetails BuildMovieDetailsModel(MovieResponse movieResponse, 
            MovieDBModel movieDbModel, Credits credits, List<ReviewDbModel> reviews)
        {
            var movieDetails = new MovieDetails();

            movieDetails.Id = movieDbModel.TMDbId;
            movieDetails.Title = movieResponse.title;
            movieDetails.Rating = movieDbModel.Rating;
            movieDetails.PosterPath = movieResponse.poster_path;
            movieDetails.RunTime = movieResponse.runtime;
            movieDetails.Description = movieResponse.overview;
            movieDetails.Cast = MapCastResponseToCast(credits.cast);
            movieDetails.Crew = MapCrewResponseToCrew(credits.crew);
            movieDetails.Genres = movieResponse.genres.Select(g => g.name).ToList();
            movieDetails.Reviews = MapReviewDbModelToReview(reviews);
            movieDetails.RatingsCount = movieDbModel.RatingsCount;
            try
            {
                movieDetails.ReleaseDate = DateTime.ParseExact(movieResponse.release_date, "yyyy-mm-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch { }

            return movieDetails;
        }

        public static List<PersonListModel> MapToPersonModel(PersonListResponse personResponse)
        {
            var personListItem = new List<PersonListModel>();

            foreach (var person in personResponse.results)
            {
                personListItem.Add(
                    new PersonListModel()
                        {
                            Id = person.id,
                            FullName = person.name,
                            PosterPath = "http://image.tmdb.org/t/p/w185" + person.profile_path
                        }
                    );
            }

            return personListItem;
        }

        public static PersonModel MapToPersonModel(PersonResponse person)
        {
            return new PersonModel()
            {
                Id = person.id,
                Job = person.known_for_department,
                Biography = person.biography,
                Description = person.biography.Substring(0, person.biography.IndexOf('.', person.biography.IndexOf('.') + 1) + 1),
                FullName = person.name,
                PlaceOfBirth = person.place_of_birth,
                PosterPath = "http://image.tmdb.org/t/p/w185" + person.profile_path
            };
        }

        public static void AddMoviesToPersonDetails(PersonResponse foundPerson, PersonModel personDetails)
        {
            foreach (var movie in foundPerson.known_for)
            {
                personDetails.Filmography.Add(new PersonModel.MovieForPerson() {
                    Id = movie.id,
                    MovieName = movie.original_title,
                    Year = DateTime.ParseExact(movie.release_date, "yyyy-mm-dd",
                            CultureInfo.InvariantCulture).Year,
                    CharacterImg = "http://image.tmdb.org/t/p/w185" + movie.poster_path
                });
            }
        }

        public static void AddImagesToPersonDetails(List<string> images, PersonModel personDetails)
        {
            foreach (var path in images)
            {
                personDetails.Images.Add("http://image.tmdb.org/t/p/w185" + path);
            }
        }

        public static void AddDetailsToPerson(PersonListModel personModel, PersonResponse personResponse)
        {
            personModel.Description = personResponse.biography;
            personModel.Job = personResponse.known_for_department;
            personModel.PlaceOfBirth = personResponse.known_for_department;
        }

        private static List<Review> MapReviewDbModelToReview(List<ReviewDbModel> dbReviews)
        {
            List<Review> reviews = new List<Review>();

            foreach (var review in dbReviews)
            {
                reviews.Add(
                    new Review()
                    {
                        Description = review.Text,
                        Title = review.Title,
                        UserId = review.UserId,
                        User = review.Username,
                        
                    });
            }

            return reviews;
        }

        private static List<Cast> MapCastResponseToCast(List<CastResponse> cast)
        {
            var castList = new List<Cast>();

            foreach (var castPerson in cast)
            {
                castList.Add(
                    new Cast()
                    {
                        CreditId = castPerson.credit_id,
                        CastId = castPerson.cast_id,
                        Character = castPerson.character,
                        Order = castPerson.order,
                        Gender = castPerson.gender,
                        Id = castPerson.id,
                        Name = castPerson.name,
                        ProfilePath = castPerson.profile_path
                    });
            }

            return castList;
        }

        private static List<Crew> MapCrewResponseToCrew(List<CrewResponse> cast)
        {
            var crewList = new List<Crew>();

            foreach (var crewPerson in cast)
            {
                crewList.Add(
                    new Crew()
                    {
                        CreditId = crewPerson.credit_id,
                        Department = crewPerson.department,
                        Gender = crewPerson.gender,
                        Id = crewPerson.id,
                        Job = crewPerson.job,
                        Name = crewPerson.name,
                        ProfilePath = crewPerson.profile_path
                    });
            }

            return crewList;
        }

        private static List<CrewResponse> GetDirectorsFromCredits(Credits credits)
        {
            var directors = new List<CrewResponse>();

            foreach (var crewMember in credits.crew)
            {
                if (crewMember.job == "Director")
                    directors.Add(crewMember);
            }

            return directors;
        }
    }
}
