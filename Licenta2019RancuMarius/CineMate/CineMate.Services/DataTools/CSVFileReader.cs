﻿using CineMate.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CineMate.Services.DataTools
{
    public class CSVFileReader
    {
        public static List<MovieDBModel> GetMoviesFromFile(string filePath, string imdbIdsFile)
        {
            var movies = new List<MovieDBModel>();
            using (var reader = new StreamReader(filePath))
            {
                var isFirstLine = true;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (isFirstLine)
                    {
                        isFirstLine = false;
                        continue;
                    }
                    var values = line.Split(',');

                    movies.Add(new MovieDBModel 
                    {
                        Id = int.Parse(values[0]),
                        Title = values[1],
                        TMDbId = 0
                    });
                }
            }

            using (var reader = new StreamReader(imdbIdsFile))
            { 
                var isFirstLine = true;
                MovieDBModel foundMovie;
                string[] values;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (isFirstLine)
                    {
                        isFirstLine = false;
                        continue;
                    }
                    values = line.Split(',');

                    foundMovie = movies.Find(m => m.Id == int.Parse(values[0]));
                    if(!string.IsNullOrEmpty(values[2]))
                        foundMovie.TMDbId = int.Parse(values[2]);
                }
            }
            
            return movies;
        }

        public static List<UserRating> GetUserRatingsFromFile(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            {
                var usersRatings = new List<UserRating>();
                var isFirstLine = true;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (isFirstLine)
                    {
                        isFirstLine = false;
                        continue;
                    }

                    var values = line.Split(',');
                    usersRatings.Add( new UserRating 
                    {
                        UserId = values[0].ToString(),
                        MovieId = int.Parse(values[1]),
                        Rating = decimal.Parse(values[2])
                    });
                }
                return usersRatings;

            }
        }
    }
      
}
