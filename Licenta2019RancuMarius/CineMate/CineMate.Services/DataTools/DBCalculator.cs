﻿using CineMate.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CineMate.Services.DataTools
{
    public static class DBCalculator
    {
        public static void CalculateMoviesAverageRatings(IUnitOfWork unitOfWork,  IMovieRepository movieRepo, IRatingRepository ratingRepo)
        {
            var moviesIds = movieRepo.GetAllMovies()
                .Select(m => m.Id).ToList();
            var usersRatings = ratingRepo.GetAllUsersRatings();
            List<decimal> userRatings;
            foreach (var movieId in moviesIds)
            {
                userRatings = usersRatings.Where(ur => ur.MovieId == movieId).Select(r => r.Rating).ToList();
                int ratingsCount = userRatings.Count;
                if (ratingsCount == 0) continue;
                decimal averageRating = userRatings.Sum() / ratingsCount;

                movieRepo.SetRatingFields(movieId, averageRating, ratingsCount);
            }

            unitOfWork.Commit();
        }

        public static void PopulateMoviesTable(IMovieRepository movieRepo)
        {
            var movies = CSVFileReader.GetMoviesFromFile(@"C:\Users\mrancu\Desktop\ml-20m\movies.csv", @"C:\Users\mrancu\Desktop\ml-20m\links.csv");

            movieRepo.AddMovies(movies);
        }

        public static void PopulateUserRatingsTable(IRatingRepository ratingRepo, IUnitOfWork unitOfWork)
        {
            var usersRatings = CSVFileReader.GetUserRatingsFromFile(@"C:\Users\mrancu\Desktop\ml-20m\ratings.csv");
            ratingRepo.AddUsersRatings(usersRatings);

            unitOfWork.Commit();
        }
    }
}
