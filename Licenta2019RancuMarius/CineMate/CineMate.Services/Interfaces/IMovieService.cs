﻿using CineMate.Domain;
using CineMate.Domain.ServiceModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CineMate.DataAccess.Interfaces
{
    public interface IMovieService
    {
        Task<List<MovieListModel>> GetTopRatedMovies(int topNumber);

        Task<MovieDetails> GetMovieDetailsByIdAsync(int id, string userId);

        void AddUserRating(UserRating rating);

        void AddReviewToMovie(Review review);

        Task<List<MovieListModel>> GetRecommendedMoviesForUser(string userId, int numberOfSuggestions);

        Task<List<MovieListFavoriteModel>> GetSavedMoviesForUserAsync(string userId);

        Task<List<MovieListFavoriteModel>> GetFavoriteMoviesForUserAsync(string userId);

        void AddMovieToFavorites(string userId, int movieId);

        void AddMovieToSaved(string userId, int movieId);

        void RemoveSavedMovie(string userId, int movieId);

        void RemoveFavoriteMovie(string userId, int movieId);

        Task<List<MovieListModel>> SearchForMovie(string searchQuery);
    }
}
