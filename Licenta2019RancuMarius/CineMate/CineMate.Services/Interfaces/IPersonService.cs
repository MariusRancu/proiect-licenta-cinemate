﻿using CineMate.APIWrapper.Models;
using CineMate.Domain.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CineMate.Services.Interfaces
{
    public interface IPersonService
    {
        Task<List<PersonListModel>> GetPopularPersonsAsync();

        Task<PersonModel> GetPersonDetails(string name);
    }
}
