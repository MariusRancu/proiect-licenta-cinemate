﻿using CineMate.APIWrapper;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using CineMate.Domain.ServiceModels;
using RecommenderSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CineMate.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepo;
        private readonly IRatingRepository _ratingRepo;
        private readonly IReviewRepository _reviewRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly MovieAPIService _apiService; 
        private readonly IRecommender _recommender;
        private readonly ISavedMoviesRepository _savedMoviesRepo;
        private readonly IFavoriteMoviesRepository _favoriteMoviesRepo;

        public MovieService(IUnitOfWork unitOfWork, IMovieRepository movieRepo,
                            IRatingRepository ratingRepo, IReviewRepository reviewRepo,
                            IRecommender recommender, ISavedMoviesRepository savedMoviesRepo,
                            IFavoriteMoviesRepository favoriteMoviesRepo)
        {
            _movieRepo = movieRepo;
            _ratingRepo = ratingRepo;
            _reviewRepo = reviewRepo;
            _unitOfWork = unitOfWork;
            _savedMoviesRepo = savedMoviesRepo;
            _favoriteMoviesRepo = favoriteMoviesRepo;
            _recommender = recommender;
            _apiService = new MovieAPIService();
        }

        public void AddUserRating(UserRating newRating)
        {
            var movie = _movieRepo.GetMovieById(newRating.MovieId);

            var userRating = _ratingRepo.GetUserRatingForMovie(newRating.UserId, newRating.MovieId);
            var isMovieRatedByUser = userRating != null;
            decimal newRatingValue;
            int ratingsCount;
            if (!isMovieRatedByUser)
            {
                _ratingRepo.AddUserRating(newRating);
                newRatingValue = CalculateNewRatingValue(movie, newRating);
                ratingsCount = movie.RatingsCount + 1;
            }
            else
            {
                _ratingRepo.UpdateUserRating(userRating.Id, newRating);
                newRatingValue = CalculateUpdatedRatingValue(movie, userRating.Rating, newRating);
                ratingsCount = movie.RatingsCount;
            }

            _movieRepo.SetRatingFields(movie.Id, newRatingValue, ratingsCount);

            _unitOfWork.Commit();
        }

        private decimal CalculateUpdatedRatingValue(MovieDBModel movie, decimal oldRating, UserRating rating)
        {
            return ((movie.Rating * movie.RatingsCount) - oldRating + rating.Rating) / movie.RatingsCount;
        }

        private decimal CalculateNewRatingValue(MovieDBModel movie, UserRating addedRating)
        {
            return ((movie.Rating * movie.RatingsCount) + addedRating.Rating) / (movie.RatingsCount + 1);
        }

        public async Task<MovieDetails> GetMovieDetailsByIdAsync(int movieId, string userId)
        {
            var dbMovie = _movieRepo.GetMovieById(movieId);
            var movieResponse = await _apiService.GetMovieByIdAsync(dbMovie.TMDbId);
            var credits = await _apiService.GetCreditsById(dbMovie.TMDbId);
            var reviews = _reviewRepo.GetReviewsForMovie(movieId).ToList();
            var movieDetails = Builder.BuildMovieDetailsModel(movieResponse, 
                                                             dbMovie, credits, reviews);

            if(!string.IsNullOrEmpty(userId))
            {
                movieDetails.UserRating = _ratingRepo.GetUserRatingForMovie(userId, movieId)?.Rating;
            }

            var movieImagesResponse = await _apiService.GetImagesPathsForMovie(dbMovie.TMDbId);
            if(movieImagesResponse != null)
            {
                movieDetails.ImagesPaths = movieImagesResponse
                              .backdrops.Select(img => "http://image.tmdb.org/t/p/w185" 
                                + img.file_path).ToList();
            }

            var videos = await _apiService.GetVideosForMovie(dbMovie.TMDbId);

            if(videos != null)
            {
                movieDetails.VideosPaths = new List<Video>();
                foreach (var result in videos.results)
                {
                    movieDetails.VideosPaths.Add(
                        new Domain.ServiceModels.Video()
                        {
                            Key = result.key,
                            Name = result.name,
                            Site = result.site,
                            Type = result.site
                        });
                }
            }

            return movieDetails; 
        }

        public async Task<List<MovieListModel>> GetTopRatedMovies(int topNumber)
        {
            var dbMovies = _movieRepo.GetAllMovies().Where(m => m.RatingsCount > 10000)
                .OrderByDescending(m => m.Rating).Take(topNumber).ToList();
            var movieList = new List<MovieListModel>();
            foreach (var dbMovie in dbMovies)
            {
                var movieResponse = await _apiService.GetMovieByIdAsync(dbMovie.TMDbId);
                var credits = await _apiService.GetCreditsById(dbMovie.TMDbId);
                var movieListModel = Builder.BuildMovieListModel(movieResponse, dbMovie, credits);
                movieList.Add(movieListModel);
            }

            return movieList;
        }

        public void AddReviewToMovie(Review review)
        {
            var reviewDbModel = new ReviewDbModel()
            {
                MovieId = review.MovieId,
                PostDate = DateTime.Now,
                Text = review.Description,
                UserId = review.UserId,
                Username = review.User,
                Title = review.Title
            };

            _reviewRepo.AddReview(reviewDbModel);
            _unitOfWork.Commit();
        }

        public async Task<List<MovieListModel>> GetRecommendedMoviesForUser(string userId, int numberOfSuggestions)
        {
            var suggestions = _recommender.GetSuggestedMoviesForUser(userId, numberOfSuggestions);

            List<MovieDBModel> dbMovies = new List<MovieDBModel>();
            foreach (var suggestion in suggestions)
            {
                dbMovies.Add(_movieRepo.GetMovieById(suggestion.MovieId));
            }

            var movieList = new List<MovieListModel>();
            foreach (var dbMovie in dbMovies)
            {
                var movieResponse = await _apiService.GetMovieByIdAsync(dbMovie.TMDbId);
                var credits = await _apiService.GetCreditsById(dbMovie.TMDbId);
                var movieListModel = Builder.BuildMovieListModel(movieResponse, dbMovie, credits);
                movieList.Add(movieListModel);
            }

            return movieList;
        }

        public async Task<List<MovieListFavoriteModel>> GetSavedMoviesForUserAsync(string userId)
        {
            var savedMovies = _savedMoviesRepo.GetSavedMoviesForUser(userId).Select(sm => sm.Movie).ToList();

            var movieList = new List<MovieListFavoriteModel>();
            foreach (var dbMovie in savedMovies)
            {
                var movieResponse = await _apiService.GetMovieByIdAsync(dbMovie.TMDbId);
                var credits = await _apiService.GetCreditsById(dbMovie.TMDbId);
                var userReview = _reviewRepo.GetReviewsForMovie(dbMovie.Id).Where( m => m.UserId == userId).FirstOrDefault();
                var userRating = _ratingRepo.GetUserRatingForMovie(userId, dbMovie.Id);
                var movieListModel = Builder.BuildFavoriteMovieListModel(movieResponse, dbMovie, credits, userReview, userRating?.Id);

                movieList.Add(movieListModel);
            }

            return movieList;
        }

        public async Task<List<MovieListFavoriteModel>> GetFavoriteMoviesForUserAsync(string userId)
        {
            var favoriteMovies = _favoriteMoviesRepo.GetFavoriteMoviesForUser(userId).Select(sm => sm.Movie).ToList();

            var movieList = new List<MovieListFavoriteModel>();
            foreach (var dbMovie in favoriteMovies)
            {
                var movieResponse = await _apiService.GetMovieByIdAsync(dbMovie.TMDbId);
                var credits = await _apiService.GetCreditsById(dbMovie.TMDbId);
                var userReview = _reviewRepo.GetReviewsForMovie(dbMovie.Id).Where(m => m.UserId == userId).FirstOrDefault();
                var userRating = _ratingRepo.GetUserRatingForMovie(userId, dbMovie.Id);

                var movieListModel = Builder.BuildFavoriteMovieListModel(movieResponse, dbMovie, credits, userReview, userRating?.Id);

                movieList.Add(movieListModel);
            }

            return movieList;
        }

        public void AddMovieToFavorites(string userId, int movieId)
        {
            var dbMovie = _movieRepo.GetMovieById(movieId);
            _favoriteMoviesRepo.AddMovieToFavorites(userId, dbMovie);

            _unitOfWork.Commit();
        }

        public void AddMovieToSaved(string userId, int movieId)
        {
            var dbMovie = _movieRepo.GetMovieById(movieId);
            _savedMoviesRepo.AddMovieToSaved(userId, dbMovie);

            _unitOfWork.Commit();
        }

        public void RemoveSavedMovie(string userId, int movieId)
        {
            var dbMovie = _movieRepo.GetMovieById(movieId);
            var savedMovie = _savedMoviesRepo.GetSavedMovie(userId, dbMovie);
            _savedMoviesRepo.DeleteSavedMovie(savedMovie);
            _unitOfWork.Commit();
        }

        public void RemoveFavoriteMovie(string userId, int movieId)
        {
            var dbMovie = _movieRepo.GetMovieById(movieId);
            var favoriteMovie = _favoriteMoviesRepo.GetFavoriteMovie(userId, dbMovie);
            _favoriteMoviesRepo.DeleteFavoriteMovie(favoriteMovie);
            _unitOfWork.Commit();
        }

        public async Task<List<MovieListModel>> SearchForMovie(string searchQuery)
        {
            var foundDBMovies = _movieRepo.GetAllMovies().Where(m => m.Title.ToUpper().Contains(searchQuery.ToUpper()));
            var movieList = new List<MovieListModel>();

            foreach (var movie in foundDBMovies)
            {
                var movieResponse = await _apiService.GetMovieByIdAsync(movie.TMDbId);
                if(movieResponse == null) continue;

                var credits = await _apiService.GetCreditsById(movie.TMDbId);
                var movieListModel = Builder.BuildMovieListModel(movieResponse, movie, credits);
                movieList.Add(movieListModel);
            }

            return movieList;
        }
    }
}
