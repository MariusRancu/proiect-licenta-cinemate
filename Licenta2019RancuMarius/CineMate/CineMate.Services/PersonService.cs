﻿using CineMate.APIWrapper;
using CineMate.APIWrapper.Models;
using CineMate.Domain.ServiceModels;
using CineMate.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CineMate.Services
{
    public class PersonService : IPersonService
    {
        private PersonAPIService _apiService;

        public PersonService()
        {
            _apiService = new PersonAPIService();
        }

        public async Task<PersonModel> GetPersonDetails(string name)
        {
            var foundPerson = (await _apiService.FindPerson(name)).results[0];
            if(foundPerson == null) return null;

            var personDetails = await _apiService.GetPersonDetails(foundPerson.id);
            var personModel = Builder.MapToPersonModel(personDetails);
            Builder.AddMoviesToPersonDetails(foundPerson, personModel);

            var images = await _apiService.GetPersonImages(personModel.Id);
            Builder.AddImagesToPersonDetails(images, personModel);

            return personModel;
        }

        public async Task<List<PersonListModel>> GetPopularPersonsAsync()
        {
            var personsResponseList = await _apiService.GetPopularPersons();
            var personList = Builder.MapToPersonModel(personsResponseList);

            foreach (var person in personList)
            {
                var personDetails = await _apiService.GetPersonDetails(person.Id);
                Builder.AddDetailsToPerson(person, personDetails);
            }

            return personList;
        }
    }
}
