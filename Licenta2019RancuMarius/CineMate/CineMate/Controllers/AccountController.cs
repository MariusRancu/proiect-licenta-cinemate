﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CineMate.DataAccess.Identity;
using CineMate.WebAPI.Helpers;
using CineMate.WebAPI.Models;
using CineMate.WebAPI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace CineMate.WebAPI.Controllers
{
    [Route("api/[controller]/")]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IJwtFactory _jwtFactory;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
        {
           _userManager = userManager;
           _signInManager = signInManager;
           _jwtOptions = jwtOptions.Value;
           _jwtFactory = jwtFactory;
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterVm model)
        {
            var user = new AppUser() { UserName = model.Username, Email = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                return new OkObjectResult(user);
            }

            return Unauthorized();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody]Login credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var identity = await GetClaimsIdentity(credentials.Email, credentials.Password);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var user = await _userManager.FindByEmailAsync(credentials.Email);
            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, user.UserName, user.Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            await _signInManager.SignInAsync(user, isPersistent: false);

            return new OkObjectResult(jwt);
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordModel passwordModel)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;
            var user = await _userManager.FindByIdAsync(sessionUserId);
            var result = await _userManager.ChangePasswordAsync(user, passwordModel.OldPassword, passwordModel.NewPassword);

            if(result.Succeeded)
            {
                return Ok();
            }

            return BadRequest();
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByEmailAsync(email);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(email, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }

    }
}
