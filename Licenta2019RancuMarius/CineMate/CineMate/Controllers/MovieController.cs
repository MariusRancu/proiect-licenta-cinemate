﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain.ServiceModels;
using CineMate.WebAPI.Helpers;
using CineMate.WebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CineMate.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class MovieController : Controller
    {
        readonly IMovieService _movieService;
        readonly UserManager<AppUser> _userManager;

        public MovieController(IMovieService movieService, UserManager<AppUser> manager)
        {
            _movieService = movieService;
            _userManager = manager;
        }

        [HttpGet("[action]/{size}")]
        [AllowAnonymous]
        public async Task<IActionResult> List(int size, [FromQuery]string queryParams)
        {
            
            List<MovieListModel> movies;
            if (!string.IsNullOrEmpty(queryParams))
            {
                movies = await _movieService.SearchForMovie(queryParams);
            }
            else
            {
                movies = await _movieService.GetTopRatedMovies(size);
            }

            return Ok(movies);
        }

        [HttpGet("[action]/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Details(int id)
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var movie = await _movieService.GetMovieDetailsByIdAsync(id, userId);
            var movieVM = Mapper.MapMovieToMovieDetailsVM(movie);

            return Ok(movieVM);
        }

        [HttpGet("[action]")]
        [Authorize]
        public async Task<IActionResult> Recommended()
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var recommandations = await _movieService.GetRecommendedMoviesForUser(userId, 18);

            return Ok(recommandations);
        }

        [HttpGet("[action]")]
        [Authorize]
        public async Task<IActionResult> Favorite()
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var recommandations = await _movieService.GetFavoriteMoviesForUserAsync(userId);

            return Ok(recommandations);
        }

        [HttpGet("[action]")]
        [Authorize]
        public async Task<IActionResult> Saved()
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var recommandations = await _movieService.GetSavedMoviesForUserAsync(userId);

            return Ok(recommandations);
        }
        


        [HttpPost("{movieId}/[action]")]
        [Authorize]
        public IActionResult Rate(int movieId, [FromBody]Rating rating)
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var serviceRating = new Domain.UserRating()
            {
                Rating = rating.RatingValue,
                MovieId = movieId,
                UserId = userId
            };

            _movieService.AddUserRating(serviceRating);

            return Created($"movie/{movieId}/rateMovie", serviceRating);
        }


        [HttpPost("{movieId}/[action]")]
        [Authorize]
        public IActionResult Review(int movieId, [FromBody]ReviewFormModel review)
        {
            var userId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            var userReview = new Review
            {
                UserId = userId,
                User = review.Username,
                Description = review.Review,
                Title = review.Title,
                MovieId = movieId
            };

            _movieService.AddReviewToMovie(userReview);

            return Created($"movie/{movieId}/review", userReview);
        }
    }
}