﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CineMate.DataAccess.Interfaces;
using CineMate.Domain.ServiceModels;
using CineMate.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CineMate.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : Controller
    {
        IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Popular()
        {
            return Ok(await _personService.GetPopularPersonsAsync());
        }

        [HttpGet("{personName}")]
        public async Task<IActionResult> Details(string personName)
        {
            return Ok(await _personService.GetPersonDetails(personName));
        }
    }
}