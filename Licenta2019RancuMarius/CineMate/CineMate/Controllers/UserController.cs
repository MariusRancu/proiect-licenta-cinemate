﻿using System.Linq;
using System.Threading.Tasks;
using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.WebAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CineMate.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        readonly IMovieService _movieService;
        readonly UserManager<AppUser> _userManager;

        public UserController(IMovieService movieService, UserManager<AppUser> manager)
        {
            _movieService = movieService;
            _userManager = manager;
        }

        [HttpPost("{userId}/[action]")]
        [Authorize]
        public IActionResult Save(string userId, [FromBody]FavoriteMovie movie)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            if(sessionUserId != userId) return Unauthorized();

            _movieService.AddMovieToSaved(userId, movie.MovieId);

            return Created("movie/saved", movie);
        }

        [HttpGet("{userId}/[action]")]
        [Authorize]
        public async Task<IActionResult> Save(string userId)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            if (sessionUserId != userId) return Unauthorized();

            var savedMovies = await _movieService.GetSavedMoviesForUserAsync(userId);

            return Ok(savedMovies);
        }

        [HttpDelete("{userId}/[action]/{movieId}")]
        [Authorize]
        public IActionResult Favorite(string userId, int movieId)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            if (sessionUserId != userId) return Unauthorized();

            _movieService.RemoveFavoriteMovie(userId, movieId);

            return Ok(movieId);
        }

        [HttpDelete("{userId}/[action]/{movieId}")]
        [Authorize]
        public IActionResult Save(string userId, int movieId)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;

            if (sessionUserId != userId) return Unauthorized();

            _movieService.RemoveSavedMovie(userId, movieId);

            return Ok(movieId);
        }

        [HttpPost("{userId}/[action]")]
        [Authorize]
        public IActionResult Favorite(string userId, [FromBody]FavoriteMovie movie)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;
            if (sessionUserId != userId) return Unauthorized();

            _movieService.AddMovieToFavorites(userId, movie.MovieId);

            return Created($"user/favorite/{userId}", movie);
        }

        [HttpGet("{userId}/[action]")]
        [Authorize]
        public async Task<IActionResult> Favorite(string userId)
        {
            var sessionUserId = User?.Claims?.FirstOrDefault(c => c.Type == "id")?.Value;
            if (sessionUserId != userId) return Unauthorized();

            var favoriteMovies = await _movieService.GetFavoriteMoviesForUserAsync(userId);

            return Ok(favoriteMovies);
        }
    }
}