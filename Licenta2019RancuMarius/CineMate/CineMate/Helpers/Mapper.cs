﻿using CineMate.APIWrapper.Models;
using CineMate.Domain.ServiceModels;
using CineMate.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cast = CineMate.Domain.ServiceModels.Cast;
using Review = CineMate.Domain.ServiceModels.Review;
using Video = CineMate.WebAPI.ViewModels.Video;

namespace CineMate.WebAPI.Helpers
{
    public static class Mapper
    {
        public static MovieListVM MapMovieToMovieListVM(MovieListModel movie)
        {
            var movieVM = new MovieListVM();

            
            return movieVM;
        }

        public static MovieDetailsVM MapMovieToMovieDetailsVM(MovieDetails movie)
        {
            var movieDetailsVM = new MovieDetailsVM();

            movieDetailsVM.Id = movie.Id;
            movieDetailsVM.Title = movie.Title;
            movieDetailsVM.Rating = movie.Rating;
            movieDetailsVM.RatingsCount = movie.RatingsCount;
            movieDetailsVM.PosterPath = movie.PosterPath;
            movieDetailsVM.RunTime = movie.RunTime;
            movieDetailsVM.Description = movie.Description;
            movieDetailsVM.Cast = MapCastToVMCast(movie.Cast);
            movieDetailsVM.Crew = MapCrewToVMCrew(movie.Crew);
            movieDetailsVM.Directors = GetDirectorsFromCrew(movie.Crew);
            movieDetailsVM.Genres = movie.Genres;
            movieDetailsVM.ReleaseDate = movie.ReleaseDate;
            movieDetailsVM.Year = movie.ReleaseDate.Year;
            movieDetailsVM.Media = new Media();
            movieDetailsVM.Reviews = MapReviewsToReviewVM(movie.Reviews);
            foreach (var picture in movie.ImagesPaths)
            {
                movieDetailsVM.Media.Images.Add(
                    new Image()
                    {
                        Path = picture,
                        Type = "image"
                    });
            }

            foreach (var video in movie.VideosPaths)
            {
                movieDetailsVM.Media.Videos.Add(
                    new Video()
                    {
                        Path = "https://www.youtube.com/embed/" + video.Key,
                        VideoName = video.Name
                    });
            }

            return movieDetailsVM;
        }

        private static List<ViewModels.Review> MapReviewsToReviewVM(List<Review> reviews)
        {
            var vmReviews = new List<ViewModels.Review>();

            foreach (var review in reviews)
            {
                vmReviews.Add(new ViewModels.Review() 
                {
                   UserId = review.UserId,
                   User = review.User,
                   Title = review.Title,
                   Description = review.Description,
                });
            }

            return vmReviews;
        }

        private static List<ViewModels.Cast> MapCastToVMCast(List<Cast> cast)
        {
            var castList = new List<ViewModels.Cast>();

            foreach (var person in cast)
            {
                castList.Add(
                    new ViewModels.Cast() { 
                        Id = person.Id,
                        Name = person.Name,
                        ProfilePath = "http://image.tmdb.org/t/p/w185" + person.ProfilePath,
                        Persona = person.Character
                    });
            }

            return castList;
        }

        private static List<ViewModels.Person> MapCrewToVMCrew(List<Crew> crew)
        {
            var crewList = new List<ViewModels.Person>();

            foreach (var person in crew)
            {
                crewList.Add(
                    new ViewModels.Person()
                    {
                        Id = person.Id,
                        Name = person.Name,
                        ProfilePath = "http://image.tmdb.org/t/p/w185" + person.ProfilePath
                    });
            }

            return crewList;
        }

        private static List<Person> GetDirectorsFromCrew(List<Crew> crewList)
        {
            var directors = new List<Person>();

            foreach (var person in crewList)
            {
                if(person.Job == "Director")
                {
                    directors.Add(new Person() {
                        Name = person.Name,
                        ProfilePath = "http://image.tmdb.org/t/p/w185" + person.ProfilePath
                    });
                }
            }

            return directors;
        }
    }
}
