﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CineMate.WebAPI.Models
{
    public class Rating
    {
        public decimal RatingValue { get; set; }
    }
}
