﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CineMate.WebAPI.Models
{
    public class ReviewFormModel
    {
        public string Username { get; set; }

        public string Title { get; set; }

        public string Review { get; set; }
    }
}
