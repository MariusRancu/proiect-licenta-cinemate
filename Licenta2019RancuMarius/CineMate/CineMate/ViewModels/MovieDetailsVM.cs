﻿using System;
using System.Collections.Generic;

namespace CineMate.WebAPI.ViewModels
{
    public class MovieDetailsVM
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int Year { get; set; }

        public decimal Rating { get; set; }

        public int RatingsCount { get; set; }

        public string BackgroundImgPath { get; set; }

        public string PosterPath { get; set; }

        public string Description { get; set; }

        public List<Cast> Cast { get; set; }

        public List<Person> Crew { get; set; }

        public List<Person> Directors { get; set; }

        public List<string> Genres { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int RunTime { get; set; }

        public List<string> Keywords { get; set; }

        public List<AsideDetail> AsideDetails { get; set; }

        public List<Review> Reviews { get; set; }

        public Media Media { get; set; }
    }

    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ProfilePath { get; set; }
    }

    public class Cast : Person
    {
        public string Persona { get; set; }

        public string Avatar { get; set; }
    }

    public enum AsideDetailItemType
    {
        LIST,
        STRING,
        KEYWORD
    }

    public class AsideDetail
    {
        public string EntityName { get; set; }

        public List<string> Data { get; set; }

        public AsideDetailItemType Type { get; set; }
    }

    public class Media
    {
        public List<Image> Images { get; set; }

        public List<Video> Videos { get; set; }

        public Media()
        {
            Images = new List<Image>();
            Videos = new List<Video>();
        }
    }

    public class Image
    {
        public string Path { get; set; }

        public string Type { get; set; }
    }

    public class Video
    {
        public string Path { get; set; }

        public string VideoName { get; set; }

        public string Duration { get; set; }
    }

    public class Review
    {
        public string Title { get; set; }

        public string User { get; set; }

        public string UserId { get; set; }

        public string Description { get; set; }
    }
}
