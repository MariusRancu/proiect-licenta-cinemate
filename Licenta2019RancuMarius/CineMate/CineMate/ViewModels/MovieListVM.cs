﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CineMate.WebAPI.ViewModels
{
    public class MovieListVM
    {
        public string Title { get; set; }

        public decimal Rating { get; set; }

        public string PosterPath { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int Runtime { get; set; }

        public string TagLine { get; set; }

        public List<string> Directors { get; set; }
    }
}
