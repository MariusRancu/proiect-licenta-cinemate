﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CineMate.WebAPI.ViewModels
{
    public class RegisterVm
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

    }
}
