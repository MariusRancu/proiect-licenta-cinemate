﻿using CineMate.DataAccess;
using CineMate.DataAccess.Identity;
using CineMate.DataAccess.Interfaces;
using CineMate.DataAccess.Repositories;
using CineMate.Domain;
using CineMate.Services.DataTools;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleProject
{
    class Program
    {
        public static void Main(string[] args)
        {
            var connectionString = @"Data Source=EN617270\SQLEXPRESS;Initial Catalog=CineMateDb;Integrated Security=True";

            var serviceProvider = new ServiceCollection()
            .AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString))
            .AddScoped<IMovieRepository, MovieRepository>()
            .AddScoped<IReviewRepository, ReviewRepository>()
            .AddScoped<IRatingRepository, RatingCachedRepositoryDecorator>()
            .AddScoped<IUnitOfWork, UnitOfWork>()
            .AddScoped<RatingRepository>()
            .AddMemoryCache()
            .BuildServiceProvider();

            //DBCalculator.PopulateMoviesTable(serviceProvider.GetService<IMovieRepository>());
            //DBCalculator.PopulateUserRatingsTable(serviceProvider.GetService<IRatingRepository>(), serviceProvider.GetService<IUnitOfWork>());

            //InsertAsync().Wait();

            DBCalculator.CalculateMoviesAverageRatings(
                serviceProvider.GetService<IUnitOfWork>(), 
                serviceProvider.GetService<IMovieRepository>(),
                serviceProvider.GetService<IRatingRepository>());



        }

        public static async Task InsertAsync(CancellationToken ct = default(CancellationToken))
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = @"Data Source=EN617270\SQLEXPRESS;Initial Catalog=CineMateDb;Integrated Security=True";
                await connection.OpenAsync(ct);
                using (var bulk = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, null))
                {
                    var userRatings = CSVFileReader.GetUserRatingsFromFile(@"C:\Users\mrancu\Desktop\ml-20m\ratings.csv");

                    using (var enumerator = userRatings.GetEnumerator())
                    using (var ratingReader = new ObjectDataReader<UserRating>(enumerator))
                    {
                        bulk.DestinationTableName = "UsersRatings";
                        bulk.ColumnMappings.Add(nameof(UserRating.Id), "Id");
                        bulk.ColumnMappings.Add(nameof(UserRating.UserId), "UserId");
                        bulk.ColumnMappings.Add(nameof(UserRating.MovieId), "MovieId");
                        bulk.ColumnMappings.Add(nameof(UserRating.Rating), "Rating");

                        bulk.EnableStreaming = true;
                        bulk.BatchSize = 10000;
                        bulk.NotifyAfter = 1000;
                        bulk.SqlRowsCopied += (sender, e) => Console.WriteLine("RowsCopied: " + e.RowsCopied);

                        await bulk.WriteToServerAsync(ratingReader, ct);
                    }
                }
            }
        }
    }
}
