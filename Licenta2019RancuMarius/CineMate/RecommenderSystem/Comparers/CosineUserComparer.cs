﻿using RecommenderSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Comparers
{
    public class CosineUserComparer : IComparer
    {
        public double CompareValues(List<decimal> vector1, List<decimal> vector2)
        {
            double sumProduct = 0.0;
            double sumOneSquared = 0.0;
            double sumTwoSquared = 0.0;

            var minCount = ((vector2.Count < vector1.Count) ? vector2.Count : vector1.Count);
            for (int i = 0; i < minCount; i++)
            {
                sumProduct += (double)vector1[i] * (double)vector2[i];
                sumOneSquared += Math.Pow((double)vector1[i], 2);
                sumTwoSquared += Math.Pow((double)vector2[i], 2);
            }

            return sumProduct / (Math.Sqrt(sumOneSquared) * Math.Sqrt(sumTwoSquared));
        }
    }
}
