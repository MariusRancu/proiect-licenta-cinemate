﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Interfaces
{
    public interface IComparer
    {
        double CompareValues(List<decimal> vector1, List<decimal> vector2); 
    }
}
