﻿using RecommenderSystem.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Interfaces
{
    public interface IRecommender
    {
        List<Suggestion> GetSuggestedMoviesForUser(string userId, int numberOfSugguestion);
    }
}
