﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Models
{
    public class Rating
    {
        public int RatingId { get; set; }

        public int MovieId { get; set; }

        public decimal RatingValue { get; set; }
    }

}
