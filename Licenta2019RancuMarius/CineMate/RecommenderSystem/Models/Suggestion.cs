﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Models
{
    public class Suggestion
    {
        public string UserId { get; }

        public int MovieId { get; }

        public double Score { get; }

        public Suggestion(string userId, int movieId, double score)
        {
            UserId = userId;
            MovieId = movieId;
            Score = score;
        }
    }
}
