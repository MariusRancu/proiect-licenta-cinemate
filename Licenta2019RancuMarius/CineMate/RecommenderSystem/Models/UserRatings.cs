﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecommenderSystem.Models
{
    public class UserRatings
    {
        public string UserId { get; set; }

        public List<Rating> Ratings { get; set; }

        public double Score { get; set; }

        public UserRatings()
        {
            Ratings = new List<Rating>();
        }
    }
}
