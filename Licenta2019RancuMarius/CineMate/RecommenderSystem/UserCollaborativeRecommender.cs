﻿using CineMate.DataAccess.Interfaces;
using CineMate.Domain;
using RecommenderSystem.Comparers;
using RecommenderSystem.Interfaces;
using RecommenderSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecommenderSystem
{
    public class UserCollaborativeRecommender : IRecommender
    {
        IRatingRepository _ratingRepo;
        IComparer _comparer;

        public UserCollaborativeRecommender(IRatingRepository ratingRepo, IComparer comparer)
        {
            _comparer = comparer;
            _ratingRepo = ratingRepo;
        }

        public List<Suggestion> GetSuggestedMoviesForUser(string userId, int numberOfSuggestions)
        {
            List<UserRating> ratings = _ratingRepo.GetAllUsersRatings().ToList();
            var allUsers = ratings.Select(r => r.UserId).Distinct().ToList();
            var allRatedMovies = ratings.Select(r => r.MovieId).Distinct().ToList();
            Dictionary<string, UserRatings> usersRatings = BuildUsersRatings(ratings, allUsers);

            var neighbors = GetNearestNeighborsOfUser(userId, 10, usersRatings, allUsers);

            var userRatings = usersRatings[userId].Ratings;
            List<Suggestion> suggestedMovies = new List<Suggestion>();
            foreach (var movieId in allRatedMovies)
            {
                if(userRatings.Find(r => r.MovieId == movieId) == null)
                {
                    double score = 0.0;
                    int count = 0;
                    for (int i=0; i<neighbors.Count; i++)
                    {
                        var neighborRatingForMovie = neighbors[i].Ratings.Find(r => r.MovieId == movieId);
                        if (neighborRatingForMovie != null)
                        {
                            score += (double)neighborRatingForMovie.RatingValue;
                            count++;
                        }
                    }
                    if(count > 0)
                    {
                        score = score/count;
                    }

                    suggestedMovies.Add(new Suggestion(userId, movieId, score));
                }
            }

            suggestedMovies.Sort((c, n) => n.Score.CompareTo(c.Score));

            return suggestedMovies.Take(numberOfSuggestions).ToList();
        }

        private Dictionary<string, UserRatings> BuildUsersRatings(List<UserRating> dbRatings, List<string> users)
        {
            Dictionary<string, UserRatings> usersRatings = new Dictionary<string, UserRatings>();

            foreach (var userId in users)
            {
                usersRatings.Add(userId, new UserRatings());
            }

            foreach (var rating in dbRatings)
            {
                usersRatings[rating.UserId].Ratings.Add(new Rating {
                    RatingId = rating.Id,
                    MovieId = rating.MovieId,
                    RatingValue = rating.Rating
                });
                usersRatings[rating.UserId].UserId = rating.UserId;
            }

            return usersRatings;
        }


        List<UserRatings> GetNearestNeighborsOfUser(string userId, int numberOfNeighbors, 
            Dictionary<string, UserRatings> usersRatings, List<string> users)
        {
            var neighbors = new List<string>();
            
            foreach (var currentUserId in users)
            {
                if (currentUserId == userId)
                {
                    usersRatings[currentUserId].Score = double.NegativeInfinity;
                }
                else
                {
                    usersRatings[currentUserId].Score = (double)_comparer.CompareValues(
                        usersRatings[currentUserId].Ratings.Select(r => r.RatingValue).ToList(), 
                        usersRatings[userId].Ratings.Select(r => r.RatingValue).ToList());
                }
            }

            var similarUsers = usersRatings.Values.OrderByDescending(u => u.Score);

            return similarUsers.Take(numberOfNeighbors).ToList();
        }
    }
}
