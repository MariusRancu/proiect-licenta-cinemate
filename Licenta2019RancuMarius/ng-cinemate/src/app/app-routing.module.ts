import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { MovieComponent } from './movie/movie/movie.component';
import { MovieListMainPageComponent } from './movie-list/movie-list-main-page/movie-list-main-page.component';
import { CelebrityComponent } from './celebrity/celebrity.component';
import { userRoute } from './user/user/user.route';
import { CelebrityListComponent } from './celebrity-list/celebrity-list.component';

const routes: Routes = [
  {
    path: 'index',
    component: MainPageComponent
  },
  {
    path: 'list',
    component: MovieListMainPageComponent
  },
  {
    path: 'celebrity-list',
    component: CelebrityListComponent
  },
  {
    path: 'movies/:id',
    component: MovieComponent
  },
  ...userRoute,
  {
    path: 'celebrity/:fullName',
    component: CelebrityComponent
  },
  { path: '', redirectTo: '/index', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
