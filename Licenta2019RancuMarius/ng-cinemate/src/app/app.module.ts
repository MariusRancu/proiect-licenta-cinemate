import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MainPageComponent } from './main-page/main-page.component';
import { MovieModule } from './movie/movie.module';
import { UserModule } from './user/user.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MovieListModule } from './movie-list/movie-list.module';
import { JwtInterceptor } from './services/interceptors/JWTInterceptor';
import { CelebrityComponent } from './celebrity/celebrity.component';
import { CelebrityListComponent } from './celebrity-list/celebrity-list.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    HeaderComponent,
    FooterComponent,
    CelebrityComponent,
    CelebrityListComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    MovieModule,
    UserModule,
    HttpClientModule,
    MovieListModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
