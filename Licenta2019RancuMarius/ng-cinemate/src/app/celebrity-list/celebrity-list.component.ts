import { Component, OnInit } from '@angular/core';
import {CelebrityModel} from '../celebrity/celebrity.model';
import {CelebrityListService} from './celebrity-list.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-celebrity-list',
  templateUrl: './celebrity-list.component.html',
  styleUrls: ['./celebrity-list.component.scss']
})
export class CelebrityListComponent implements OnInit {
  sortOrder: number;
  sortField: string;
  celebrities: CelebrityModel[];

  constructor(
    private celebrityService: CelebrityListService,
    private router: Router
  ) { }

  ngOnInit() {
    this.celebrityService.getCelebrityList().subscribe( (res) => {
      this.celebrities = res;
    });
  }

  onClickCelebrityDetails(fullName) {
    this.router.navigate(['celebrity/', fullName]);
  }

  onSortChange(event) {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

}
