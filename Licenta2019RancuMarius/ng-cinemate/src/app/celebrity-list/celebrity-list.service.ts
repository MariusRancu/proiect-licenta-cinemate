import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CelebrityModel} from '../celebrity/celebrity.model';

@Injectable({
  providedIn: 'root'
})
export class CelebrityListService  {
  private baseUrl = "http://localhost:60651/api/person/";

  private actions = { 
    popular: "popular" 
  };

  constructor(
    private http: HttpClient
  ) { }


  public getCelebrityList(): Observable<CelebrityModel[]> {
    return this.http.get<CelebrityModel[]>(this.baseUrl + this.actions.popular);
  }

  public getCelebrityDetails(personName: string): Observable<CelebrityModel> {
    return this.http.get<CelebrityModel>(this.baseUrl + personName);
  }
}
