import { Component, OnInit } from '@angular/core';
import {CelebrityModel} from './celebrity.model';
import {SocialNetworkItem} from '../shared/social-network/social-network.component';
import {AsideDetailItemType} from '../shared/aside-details/aside-details.component';
import {DomSanitizer} from '@angular/platform-browser';
import {of, Subscription} from 'rxjs';
import { ImageType } from '../models/movie-details.model';
import { CelebrityListService } from '../celebrity-list/celebrity-list.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-celebrity',
  templateUrl: './celebrity.component.html',
  styleUrls: ['./celebrity.component.scss']
})
export class CelebrityComponent implements OnInit {
  categories: string[];
  socialNetworkItems: SocialNetworkItem[] = [
    {
      url: '#',
      icon: 'fa-heart',
      text: 'Add to favorite'
    },
    {
      url: '#',
      icon: 'fa-share',
      text: 'Share'
    }
  ];


  constructor(
    public sanitizer: DomSanitizer,
    public celebrityService: CelebrityListService,
    private route: ActivatedRoute
  ) { }
  
  //routeSub: Subscription;
  celebrityName: string;
  init: boolean = false;
  celebrity: CelebrityModel;
  
  routeSub: Subscription;

  

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      if (!this.init) {
        this.celebrityName = params['fullName'];
        this.getPerson();
      }
      this.init = true;
    });
  }

  getPerson() {
    this.celebrityService.getCelebrityDetails(this.celebrityName).subscribe( (res) => {
      this.celebrity = res;
      let index = 0;
      // TODO: a method to map the lightboxmesages when getting the celebrity data
      this.celebrity.lightboxImages = this.celebrity.images.map( (img: string) => {
        const obj = {
          source: img,
          thumbnail: img,
          title: this.celebrity.fullName + ` ${index}`
        };
        index++;
        return obj;
      });

      this.celebrity.lightboxImages = this.celebrity.lightboxImages.slice(0, 6);
    });
  }
      

  search(event) {
    of (['', 'TV Shows', 'Others']).subscribe((data) => this.categories = data);
  }

}
