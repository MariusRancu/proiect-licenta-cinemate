import {MediaType} from '../models/movie-details.model';
import {AsideDetail} from '../shared/aside-details/aside-details.component';

export interface Character {
  movieName?: string;
  year?: number;
  characterName?: string;
  characterImg?: string;
}
export interface CelebrityModel {
  id?: number,
  fullName?: string;
  job?: string;
  description?: string;
  posterPath?: string;
  images: string[];
  backgroundImg?: string;
  filmography?: Character[];
  asideDetails?: AsideDetail[];
  biography?: string;
  placeOfBirth?: string;
  lightboxImages?: any[];
}

export interface LightboxImage {
  source: string;
  thumbnail: string;
  title?: string;
}
