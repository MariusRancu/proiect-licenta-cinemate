import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { UserStateService } from '../services/user/user-state.service';
export const enum userAuthEventType {
  'SIGNUP'= 'signup',
  'LOGIN'= 'login',
  'LOGOUT'= 'logout'
}
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  visible = false;
  loginVisible = false;

  items = [
    {
      label: 'Home',
      routerLink: 'index',
      routerLinkActive: true
    },
    {
      label: 'Top Rated Movies',
      routerLink: 'list',
      routerLinkActive: true
    },
    {
      label: 'Popular Celebrities',
      routerLink: 'celebrity-list',
      routerLinkActive: true
    }
  ];

  isLoggedIn = false;
  username = '';

  constructor(
    private router: Router,
    private authService: AuthService,
    private userStateService: UserStateService
    ) {
  }

  ngOnInit() {
    this.router.events.subscribe((res) => {
      this.isLoggedIn = this.authService.getUser() != null;
      this.username = this.authService.getUserName();
    });

    this.userStateService.$userStateSubject.subscribe( (loggedIn) => {
        this.isLoggedIn = loggedIn;
        this.username = this.authService.getUserName();
        this.router.navigate(['/index']);
    });
  }

  onSignupClick(event: any, eventType: userAuthEventType): void {
    if (eventType === userAuthEventType.SIGNUP) {
      this.visible = true;
    } else if (eventType === userAuthEventType.LOGIN) {
      this.loginVisible = true;
    }
  }

  onLogoutClick() {
    this.authService.logout();
    this.isLoggedIn = this.authService.getUser() != null;
    this.username = this.authService.getUserName();
    this.router.navigate(['/index']);
  }

}
