import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ChipGenre } from '../shared/chip/chip.component';
import { SocialNetworkItem } from '../shared/social-network/social-network.component';
import { MoviesService } from '../services/movies/movies.service';
import { MovieList } from '../models/movie-list.model';
import { AuthService } from '../services/auth/auth.service';
import { UserStateService } from '../services/user/user-state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainPageComponent implements OnInit {
  items: MenuItem[];
  carouselItems: MovieList[];
  carouselRecommendedItems: MovieList[];
  carouselWithVideoItems: any[];
  spotlightRightArrow: any;
  spotlightLeftArrow: any;
  carouselInterval: any;
  newsItems: any;

  constructor(
    private moviesService: MoviesService,
    private authService: AuthService,
    private userStateService: UserStateService,
    private router: Router)
  {}

  socialNetworkItems: SocialNetworkItem[] = [
    {
      url: '#',
      icon: 'fa-heart',
      text: 'Add to favorite'
    },
    {
      url: '#',
      icon: 'fa-play',
      text: 'Watch trailer'
    }
  ];

  ngOnInit() {
    // this.newsItems = [
    //   {
    //     title: 'Godzilla: King Of The Monsters Adds O’Shea Jackson Jr',
    //     date: '27 Mar 2017',
    //     description: 'Looks like Kong: Skull Island started a tradition with its casting of Straight ...',
    //     posterPath: 'https://m.media-amazon.com/images/M/MV5BN2E4ZDgxN2YtZjExMS00MWE5LTg3NjQtNTkxMzJhOTA3MDQ4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg'
    //   },
    //   {
    //     title: 'Godzilla: King Of The Monsters Adds O’Shea Jackson Jr',
    //     date: '27 Mar 2017',
    //     description: 'Looks like Kong: Skull Island started a tradition with its casting of Straight ...',
    //     posterPath: 'https://m.media-amazon.com/images/M/MV5BN2E4ZDgxN2YtZjExMS00MWE5LTg3NjQtNTkxMzJhOTA3MDQ4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg'
    //   },
    //   {
    //     title: 'Godzilla: King Of The Monsters Adds O’Shea Jackson Jr',
    //     date: '27 Mar 2017',
    //     description: 'Looks like Kong: Skull Island started a tradition with its casting of Straight ...',
    //     posterPath: 'https://m.media-amazon.com/images/M/MV5BN2E4ZDgxN2YtZjExMS00MWE5LTg3NjQtNTkxMzJhOTA3MDQ4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg'
    //   },  {
    //     title: 'Godzilla: King Of The Monsters Adds O’Shea Jackson Jr',
    //     date: '27 Mar 2017',
    //     description: 'Looks like Kong: Skull Island started a tradition with its casting of Straight ...',
    //     posterPath: 'https://m.media-amazon.com/images/M/MV5BN2E4ZDgxN2YtZjExMS00MWE5LTg3NjQtNTkxMzJhOTA3MDQ4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg'
    //   }
    // ];

    var CarouselTitle: string;
    this.userStateService.$userStateSubject.subscribe( (loggedIn) => {
      this.getHardcodedRecommende();
    });


    this.moviesService.GetMovieList(10).subscribe(res => {
      this.carouselItems = res.map(movie => ({
        id: movie.id,
        title: movie.title,
        rating: movie.rating,
        tagline: movie.description,
        releaseDate: movie.releaseDate,
        runtime: movie.runtime,
        posterPath: "http://image.tmdb.org/t/p/w185" + movie.posterPath,
        genres: movie.genres
      }));
    });
    
    

  //   this.carouselWithVideoItems = [
  //     {
  //       title: 'Guardians of the Galaxy',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy2',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy3',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy4',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy5',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy6',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     },
  //     {
  //       title: 'Guardians of the Galaxy7',
  //       genres: [ChipGenre.ACTION, ChipGenre.SCIFI, ChipGenre.ADVENTURE],
  //       rating: 7.4,
  //       year: 2015,
  //       runTime: '2h21\'',
  //       rated: 'PG-13',
  //       releaseDate: '1 May 2015',
  //       posterPath: 'https://m.media-amazon.com/images/M/MV5BMTg2MzI1MTg3OF5BMl5BanBnXkFtZTgwNTU3NDA2MTI@._V1_.jpg'
  //     }
  //   ];
  //   this.addEventListenersToCarouselPageControls();
   }

  addEventListenersToCarouselPageControls() {
    this.carouselInterval = setInterval( () => {
      this.spotlightLeftArrow = document.querySelector('.spotlight-container .ui-carousel-prev-button');
      this.spotlightRightArrow = document.querySelector('.spotlight-container .ui-carousel-next-button');
      if (this.spotlightRightArrow && this.spotlightLeftArrow) {
        this.spotlightRightArrow.addEventListener('click', () => {
          const firstItem = this.carouselWithVideoItems[0];
          this.carouselWithVideoItems = [...this.carouselWithVideoItems.slice(1, this.carouselWithVideoItems.length), firstItem];
        });

        this.spotlightLeftArrow.addEventListener('click', () => {
        });
        clearInterval(this.carouselInterval);
      }
    });
  }

  getRecommendedMoviesForCarousel() {
    if(this.authService.getUserName() != null) {
      this.moviesService.GetMovieRecommandations().subscribe(res => {
        this.carouselRecommendedItems = res.map(movie => ({
          id: movie.id,
          title: movie.title,
          rating: movie.rating,
          tagline: movie.description,
          releaseDate: movie.releaseDate,
          runtime: movie.runtime,
          posterPath: "http://image.tmdb.org/t/p/w185" + movie.posterPath,
          genres: movie.genres
        }));
      });

    } 
  }

  getHardcodedRecommende() {
    this.carouselRecommendedItems = [{"id":147,"title":"The Basketball Diaries","rating":3.58,"releaseDate": new Date("1995-01-21T00:04:00"),"runtime":"102","posterPath":"http://image.tmdb.org/t/p/w185/8BkedYL5LdpkyHc7JtuohMr8a9N.jpg","genres":["Drama","Crime"]},{"id":289,"title":"Only You","rating":3.35,"releaseDate":new Date("1994-01-17T00:09:00"),"runtime":"115","posterPath":"http://image.tmdb.org/t/p/w185/eaHH1hnbZ9wIUHjYKlqJUwpTBan.jpg","genres":["Comedy","Romance"]},{"id":38095,"title":"A Bittersweet Life","rating":3.8,"releaseDate":new Date("2005-01-31T00:03:00"),"runtime":"120","posterPath":"http://image.tmdb.org/t/p/w185/faWrxjYaYfh9Ee4Vc3XrwoT3fOf.jpg","genres":["Action","Drama","Crime"]},{"id":73266,"title":"Youth in Revolt","rating":3.44,"releaseDate": new Date("2009-01-11T00:09:00"),"runtime":"87","posterPath":"http://image.tmdb.org/t/p/w185/nQ45Ypo7fdgBWGUICyT0XR5RPU6.jpg","genres":["Drama","Comedy","Romance"]},{"id":111,"title":"Taxi Driver","rating":4.11,"releaseDate":new Date("1976-01-07T00:02:00"),"runtime":"114","posterPath":"http://image.tmdb.org/t/p/w185/ekstpH614fwDX8DUln1a2Opz0N8.jpg","genres":["Crime","Drama"]},{"id":107,"title":"Muppet Treasure Island","rating":3.22,"releaseDate":new Date("1996-01-16T00:02:00"),"runtime":"100","posterPath":"http://image.tmdb.org/t/p/w185/5A8gKzOrF9Z7tSUX6xd5dEx4NXf.jpg","genres":["Action","Comedy","Music","Family","Adventure"]},{"id":33410,"title":"Crackerjack","rating":3.46,"releaseDate": new Date("2002-01-11T00:07:00"),"runtime":"92","posterPath":"http://image.tmdb.org/t/p/w185/vUHqBgt5e7jMmMnvP1Dk1gpnvZO.jpg","genres":["Comedy"]},{"id":474,"title":"In the Line of Fire","rating":3.73,"releaseDate": new Date("1993-01-08T00:07:00"),"runtime":"128","posterPath":"http://image.tmdb.org/t/p/w185/bmeD5LdQiRaZmVHbySXw6WJgAVA.jpg","genres":["Action","Drama","Thriller","Crime","Mystery"]},{"id":435,"title":"Coneheads","rating":2.59,"releaseDate": new Date("1993-01-23T00:07:00"),"runtime":"88","posterPath":"http://image.tmdb.org/t/p/w185/9vuFI0xV1yZfsWr23evJlkRDL8j.jpg","genres":["Comedy","Science Fiction","Family"]},{"id":8994,"title":"The Ladies Man","rating":3.12,"releaseDate": new Date("1961-01-21T00:06:00"),"runtime":"95","posterPath":"http://image.tmdb.org/t/p/w185/mWqqcCffigkt2lkmzcQsPDGTd4U.jpg","genres":["Comedy"]},{"id":432,"title":"City Slickers II: The Legend of Curly's Gold","rating":2.74,"releaseDate": new Date("1994-01-10T00:06:00"),"runtime":"116","posterPath":"http://image.tmdb.org/t/p/w185/gh0lsyJARglhpbKmnY8i8M7V5bI.jpg","genres":["Action","Comedy","Drama","Western"]},{"id":2329,"title":"American History X","rating":4.15,"releaseDate": new Date("1998-01-30T00:10:00"),"runtime":"119","posterPath":"http://image.tmdb.org/t/p/w185/fXepRAYOx1qC3wju7XdDGx60775.jpg","genres":["Drama"]},{"id":315,"title":"The Specialist","rating":2.9,"releaseDate": new Date("1994-01-07T00:10:00"),"runtime":"110","posterPath":"http://image.tmdb.org/t/p/w185/flK0OtdFRvfbs5BixEqU327H8ZB.jpg","genres":["Action","Thriller"]},{"id":48394,"title":"Pan's Labyrinth","rating":4.04,"releaseDate": new Date("2006-01-25T00:08:00"),"runtime":"118","posterPath":"http://image.tmdb.org/t/p/w185/t0TDsqbCTgSi0AL7k4baZrOYYhi.jpg","genres":["Fantasy","Drama","War"]},{"id":225,"title":"Disclosure","rating":3.29,"releaseDate": new Date("1994-01-09T00:12:00"),"runtime":"123","posterPath":"http://image.tmdb.org/t/p/w185/qbwcjmHnXkeE8l7wNBsdnlrp3J4.jpg","genres":["Drama","Thriller","Crime","Mystery","Romance"]},{"id":4619,"title":"Little Monsters","rating":2.77,"releaseDate": new Date("1989-01-25T00:08:00"),"runtime":"102","posterPath":"http://image.tmdb.org/t/p/w185/3L1yXIoue6WhXwd21deArRkHuOQ.jpg","genres":["Adventure","Fantasy","Comedy","Family"]},{"id":2428,"title":"The Faculty","rating":2.82,"releaseDate": new Date("1998-01-25T00:12:00"),"runtime":"104","posterPath":"http://image.tmdb.org/t/p/w185/6L1SgKyzDy5x2oEskUyfMcdVed2.jpg","genres":["Horror","Science Fiction"]},{"id":4622,"title":"Loverboy","rating":2.7,"releaseDate": new Date("1989-01-28T00:04:00"),"runtime":"98","posterPath":"http://image.tmdb.org/t/p/w185/xZp1Uug3uc99WQWouUcs5CWxcbw.jpg","genres":["Comedy","Romance"]}];
  }

  onClickMovieDetails(movieId) {
    this.router.navigate(['movies/', movieId]);
  }
}
