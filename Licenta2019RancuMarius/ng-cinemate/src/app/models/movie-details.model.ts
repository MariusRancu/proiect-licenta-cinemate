import { AsideDetailItemType } from "../shared/aside-details/aside-details.component";
import { ChipGenre } from '../shared/chip/chip.component';

export class MovieDetails {
    title?: string;
    year?: number;
    rating?: number;
    ratingsCount?: number;
    reviewsCount?: number;
    yourRating?: number;
    media?: MediaType;
    backgroundImg?: string;
    posterPath?: string;
    description?: string;
    cast?: Cast[]= [];
    crew?: Person[]= [];
    genres?: ChipGenre[]= [];
    releaseDate?: string;
    runTime?: number;
    mmpaRating?: string;
    keywords: string[]= [];
    asideDetails?: AsideDetail[]= [];
    reviews?: Review[] = [];
    directors?: Person[]= [];
    lightBoxImages?: LightBoxImage[] = [];
    inWatchList?: boolean;
}

export class LightBoxImage {
    public source: string;
    public thumbnail: string;
    public title: string;
}

export class MediaType {
    public images: ImageType[];
    public videos: VideoType[];
}

export class ImageType {
    public path: string;
    public type: string;
}

export class VideoType {
    public path: string;
    public videoName: string;
    public duration: string;
}

class Person {
    public name: string;
    public profilePath: string;
}

class Cast extends Person {
    public persona: string;
    public avatar: string;
}

class AsideDetail {
    entityName: string;
    data: string[];
    type: AsideDetailItemType;
}

export class Review {
    title: string;
    stars: number;
    postDate: Date;
    user: string;
    userId: number;
    description: string;
}


