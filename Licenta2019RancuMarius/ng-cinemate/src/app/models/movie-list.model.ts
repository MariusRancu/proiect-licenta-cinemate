import {Review} from './movie-details.model';

export class MovieList {
    public title?: string;
    public year?: number;
    public rating?: number;
    public description?: string;
    public posterPath?: string;
    public runtime?: string;
    public mmpaRating?: string;
    public releaseDate?: Date;
    public directors?: string[];
    public genres?: string[];
    public stars?: string[];
    public id?: number;
}

export class UserMovieList extends MovieList {
  public userRating?: number;
  public userReview?: Review;
}
