import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MovieList } from 'src/app/models/movie-list.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-list-main-page',
  templateUrl: './movie-list-main-page.component.html',
  styleUrls: ['./movie-list-main-page.component.scss']
})

export class MovieListMainPageComponent implements OnInit {
  queryString: string;
  backupQueryString: string;
  sortOrder: number;
  sortField: string;
  sortOptions = [
    {
      label: 'Popularity Descending',
      value: '!rating'
    },
    {
      label: 'Popularity Ascending',
      value: 'rating'
    },
    {
      label: 'Release Date Descending',
      value: '!releaseDate'
    },
    {
      label: 'Release Date Ascending',
      value: 'releaseDate'
    }
  ];
  sortKey: string;
  movies: MovieList[];
  pageTitle: string = "Top Rated Movies";
  constructor(
    private movieService: MoviesService,
    private router: Router,
    private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.queryString = params['queryString'];
      if(this.queryString != null) {
        this.pageTitle = "Results for: " + "\"" + this.queryString + "\"";
      }
      else {
        this.pageTitle = "Top Rated Movies";
      }
      if (this.queryString !== this.backupQueryString) {
        this.getMovieList();
      }
      this.backupQueryString = this.queryString;
    });
  }

  getMovieList() {
    this.movieService.GetMovieList(20, this.queryString).subscribe(res => {
      this.movies = res.map(movie => ({
        id: movie.id,
        title: movie.title,
        rating: movie.rating,
        tagline: movie.description,
        releaseDate: movie.releaseDate,
        runtime: movie.runtime,
        posterPath: "http://image.tmdb.org/t/p/w185" + movie.posterPath,
        directors: movie.directors
      }));
    });
  }

  ngOnInit() {
    this.getMovieList();
    //  this.movies = <any>[
    //    {
    //       "id": 278,
    //       "title": "The Shawshank Redemption",
    //       "rating": 4.45,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/9O7gLzmreU0nGkIB6K3BsJbzvNv.jpg",
    //       "releaseDate": "1994-01-23T00:09:00",
    //       "runtime": 142,
    //       "tagLine": "Fear can hold you prisoner. Hope can set you free.",
    //       "directors": [
    //          "Frank Darabont"
    //       ]
    //    },
    //    {
    //       "id": 238,
    //       "title": "The Godfather",
    //       "rating": 4.36,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/rPdtLWNsZmAtoZl9PK7S2wE3qiS.jpg",
    //       "releaseDate": "1972-01-14T00:03:00",
    //       "runtime": 175,
    //       "tagLine": "An offer you can't refuse.",
    //       "directors": [
    //          "Francis Ford Coppola"
    //       ]
    //    },
    //    {
    //       "id": 629,
    //       "title": "The Usual Suspects",
    //       "rating": 4.33,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/jgJoRWltoS17nD5MAQ1yK2Ztefw.jpg",
    //       "releaseDate": "1995-01-19T00:07:00",
    //       "runtime": 106,
    //       "tagLine": "Five Criminals. One Line Up. No Coincidence.",
    //       "directors": [
    //          "Bryan Singer"
    //       ]
    //    },
    //    {
    //       "id": 424,
    //       "title": "Schindler's List",
    //       "rating": 4.31,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/yPisjyLweCl1tbgwgtzBCNCBle.jpg",
    //       "releaseDate": "1993-01-15T00:12:00",
    //       "runtime": 195,
    //       "tagLine": "Whoever saves one life, saves the world entire.",
    //       "directors": [
    //          "Steven Spielberg"
    //       ]
    //    },
    //    {
    //       "id": 240,
    //       "title": "The Godfather: Part II",
    //       "rating": 4.28,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/bVq65huQ8vHDd1a4Z37QtuyEvpA.jpg",
    //       "releaseDate": "1974-01-20T00:12:00",
    //       "runtime": 200,
    //       "tagLine": "I don't feel I have to wipe everybody out, Tom. Just my enemies.",
    //       "directors": [
    //          "Francis Ford Coppola"
    //       ]
    //    },
    //    {
    //       "id": 567,
    //       "title": "Rear Window",
    //       "rating": 4.27,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/qitnZcLP7C9DLRuPpmvZ7GiEjJN.jpg",
    //       "releaseDate": "1954-01-01T00:08:00",
    //       "runtime": 112,
    //       "tagLine": "It only takes one witness to spoil the perfect crime.",
    //       "directors": [
    //          "Alfred Hitchcock"
    //       ]
    //    },
    //    {
    //       "id": 346,
    //       "title": "Seven Samurai",
    //       "rating": 4.27,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/v6xrz4fr92KY1oNC3HsEvrsvR1n.jpg",
    //       "releaseDate": "1954-01-26T00:04:00",
    //       "runtime": 207,
    //       "tagLine": "The Mighty Warriors Who Became the Seven National Heroes of a Small Town",
    //       "directors": [
    //          "Akira Kurosawa"
    //       ]
    //    },
    //    {
    //       "id": 289,
    //       "title": "Casablanca",
    //       "rating": 4.26,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/9UViITBCKeLYWg8Wblbmb9l25mS.jpg",
    //       "releaseDate": "1942-01-26T00:11:00",
    //       "runtime": 102,
    //       "tagLine": "They had a date with fate in Casablanca!",
    //       "directors": [
    //          "Michael Curtiz"
    //       ]
    //    },
    //    {
    //       "id": 510,
    //       "title": "One Flew Over the Cuckoo's Nest",
    //       "rating": 4.25,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/2Sns5oMb356JNdBHgBETjIpRYy9.jpg",
    //       "releaseDate": "1975-01-18T00:11:00",
    //       "runtime": 133,
    //       "tagLine": "If he's crazy, what does that make you?",
    //       "directors": [
    //          "Miloš Forman"
    //       ]
    //    },
    //    {
    //       "id": 935,
    //       "title": "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb",
    //       "rating": 4.25,
    //       "posterPath": "http://image.tmdb.org/t/p/w185/tviJ68Wj4glQk3CPMvdvExYHxX.jpg",
    //       "releaseDate": "1964-01-29T00:01:00",
    //       "runtime": 95,
    //       "tagLine": "The hot-line suspense comedy",
    //       "directors": [
    //          "Stanley Kubrick"
    //       ]
    //    }
    // ]
  }

  onClickMovieDetails(movieId) {
    this.router.navigate(['movies/', movieId]);
  }

  onSortChange(event) {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
}
