import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule, PanelModule, CardModule, ButtonModule } from 'primeng/primeng';
import { MovieListMainPageComponent } from './movie-list-main-page/movie-list-main-page.component';
import { FormsModule } from '@angular/forms';
import { DataViewModule } from 'primeng/dataview';
import { SharedModule } from '../shared/shared.module';

// const routes: Routes = [
//   {
//     path: '',
//     component: MovieListMainPageComponent
//   }
// ];

@NgModule({
  imports: [
    CommonModule,
    // RouterModule.forChild(routes),
    FormsModule,
    DropdownModule,
    DataViewModule,
    PanelModule,
    CardModule,
    ButtonModule,
    SharedModule
  ],
  declarations: [
    MovieListMainPageComponent
  ],
  exports: [
    MovieListMainPageComponent
  ]
})
export class MovieListModule { }