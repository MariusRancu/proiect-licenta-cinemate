import { NgModule } from '@angular/core';
import { MovieComponent } from './movie/movie.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [MovieComponent],
  exports: [MovieComponent]
})
export class MovieModule { }
