import {Component, OnInit} from '@angular/core';
import {of, Subscription} from "rxjs";
import {AsideDetailItemType} from 'src/app/shared/aside-details/aside-details.component';
import {MoviesService} from 'src/app/services/movies/movies.service';
import {SocialNetworkItem} from '../../shared/social-network/social-network.component';
import {MovieDetails, ImageType} from 'src/app/models/movie-details.model';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {UserService} from 'src/app/services/user/user.service';
import {AuthService} from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  commentFormVisible = false;
  sortOrder: number;
  sortField: string;
  movieId: number;
  favoriteButtonLabel = "Add to favorites";
  watchListButtonLabel = "Add to watchlist"
  isAddedToFavorites = false;
  isAddedToSaved = false;

  categories: string[];
  /**
   * MOVE TO OVERVIEW COMPONENT
   */


  // socialNetworkItems: SocialNetworkItem[] = [
  //   {
  //     url: '#',
  //     icon: 'fa-heart',
  //     text: 'Add to favorite'
  //   },
  //   {
  //     url: '#',
  //     icon: 'fa-share',
  //     text: 'Share'
  //   }
  // ];

  init = false;
  //   /**
  //  * MOVE TO OVERVIEW COMPONENT
  //  */
  movie: MovieDetails = new MovieDetails();
  routeSub: Subscription;
  userId: string;
  constructor(
    private movieService: MoviesService,
    public sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private singleMovieService: MoviesService,
    private userService: UserService,
    private authService: AuthService
  ) {
  }

  onRatingSet(event) {
    this.movieService.saveRating(this.movieId, event.value).subscribe((newRating) => {
      this.movie.ratingsCount = this.movie.ratingsCount + 1;
    });
  }

  onAddToFavorite(event) {
    this.userService.addMovieToFavorites(this.movieId, this.userId);
  }


  ngOnInit() {
    this.userId = this.authService.getUserId();
    this.routeSub = this.route.params.subscribe(params => {
      if (!this.init) {
        this.movieId = params['id'];
        this.getMovie();
      }
      this.init = true;
    });
  }

  getMovie() {
    this.movieService.GetMovieDetails(this.movieId).subscribe(res => {
      this.movie = Object.assign(new MovieDetails(), {
        title: res.title,
        rating: res.rating,
        ratingsCount: res.ratingsCount,
        description: res.description,
        releaseDate: res.releaseDate,
        year: res.year,
        runTime: res.runTime,
        posterPath: "http://image.tmdb.org/t/p/w185" + res.posterPath,
        cast: res.cast.map((c) => {
          c.profilePath = (c.profilePath !== 'http://image.tmdb.org/t/p/w185') ? c.profilePath : 'http://bestnycacupuncturist.com/wp-content/uploads/2016/11/anonymous-avatar-sm.jpg';
          return c;
        }),
        crew: res.crew.map((c) => {
          c.profilePath = (c.profilePath !== 'http://image.tmdb.org/t/p/w185') ? c.profilePath : 'http://bestnycacupuncturist.com/wp-content/uploads/2016/11/anonymous-avatar-sm.jpg';
          return c;
        }),
        directors: res.directors.map((c) => {
          c.profilePath = (c.profilePath !== 'http://image.tmdb.org/t/p/w185' && c.profilePath) ? c.profilePath : 'http://bestnycacupuncturist.com/wp-content/uploads/2016/11/anonymous-avatar-sm.jpg';
          return c;
        }),
        reviewsCount: res.reviews.length,
        yourRating: res.yourRating,
        media: res.media,
        asideDetails: [{
          entityName: 'Directors',
          data: res.directors.map(d => d.name),
          type: AsideDetailItemType.LIST
        }, {
          entityName: 'Genres',
          data: res.genres,
          type: AsideDetailItemType.LIST
        }, {
          entityName: 'Release Date',
          data: res.releaseDate,
          type: AsideDetailItemType.DATE
        }, {
          entityName: 'Runtime',
          data: res.runTime + ' minutes',
          type: AsideDetailItemType.STRING
        }],
        staff: {
          directors: res.directors,
        },
        genres: res.genres,
        reviews: res.reviews
      });
      if (this.movie.media.images && (+this.movie.media.images.length) >= 6) {
        this.movie.media.images = this.movie.media.images.slice(0, 6);
      }
      let index = 0;
      this.movie.lightBoxImages = this.movie.media.images.map((img: ImageType) => {
        let obj = {
          source: img.path,
          thumbnail: img.path,
          title: this.movie.title + ` ${index}`
        };
        index++;
        return obj;
      });
    });
  }

  search(event) {
    of(['', 'TV Shows', 'Others']).subscribe((data) => this.categories = data);
  }

  onWriteReviewClick(event) {
    this.commentFormVisible = true;
  }

  ngOnDestroy() {
    if (this.routeSub && !this.routeSub.closed) {
      this.routeSub.unsubscribe();
    }
  }

  saveMovieToSee(event) {
    
    this.userService.addMovieToSaved(this.movieId, this.userId).subscribe(
      (data) => {
        this.watchListButtonLabel = 'Added to watchlist';
        this.isAddedToSaved = true;

      }
    );
  }

  addToFavorites(event) {
    this.userService.addMovieToFavorites(this.movieId, this.userId).subscribe(
      (data) => {
        this.favoriteButtonLabel = 'Added to favorites';
        this.isAddedToFavorites = true;
      }
    )
  }

  onReviewSaved(event) {
    this.getMovie();
  }

}
