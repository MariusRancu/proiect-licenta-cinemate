import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserRegisterModel} from 'src/app/models/user-register.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) {
  }

  private baseUrl = "http://localhost:60651/api/account/";
  private actions = {
    register: "register",
    login: "login",
    changePassword: "changePassword"
  };

  register(user: UserRegisterModel): Observable<any> {
    return this.http.post(this.baseUrl + this.actions.register, user);
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.baseUrl + this.actions.login, {email, password});
  }

  changePassword(oldPassword: string, newPassword: string) {
    return this.http.post<any>(this.baseUrl + this.actions.changePassword, {oldPassword, newPassword});
  }

  getUser() {
    var user: any;

    user = JSON.parse(localStorage.getItem('currentUser'));
    return user;
  }

  getUserEmail() {
    var user = this.getUser();
    if (user == null) {
      return null;
    }
    return (user.Email);
  }

  getUserId() {
    var user = this.getUser();
    if (user == null) {
      return null;
    }
    return user.id;
  }

  getUserName() {
    var user = this.getUser();
    if (user == null) {
      return null;
    }
    return (user.Username);
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
