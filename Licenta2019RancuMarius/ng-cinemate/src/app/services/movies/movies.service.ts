import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {MovieList, UserMovieList} from 'src/app/models/movie-list.model';
import { MovieDetails } from 'src/app/models/movie-details.model';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) {}

  private baseUrl = "http://localhost:60651/api/movie/";

  private actions = {
    list: "list/",
    details: "details/",
    ratings: "rate",
    reviews: "review",
    recommandations: "recommended",
    favoriteMovies: 'favorite',
    toSeeMovies: 'toSeeMovies'
  };

  public GetMovieList(size: number, q?: string): Observable<MovieList[]> {
    let queryParams;
    if (q) {
       queryParams = new HttpParams()
      .set('queryParams', q);
    }
    return this.http.get<MovieList[]>(this.baseUrl + this.actions.list + size, {params: queryParams});
  }

  public GetMovieRecommandations(): Observable<MovieList[]> {
    return this.http.get<MovieList[]>(this.baseUrl + this.actions.recommandations);
  }
  public GetMovieDetails(id: number): Observable<MovieDetails> {
    return this.http.get<MovieDetails>(this.baseUrl + this.actions.details + id);
  }

  

  public saveRating(movieId: number, rating: number): Observable<{ratingValue: number}> {
    return this.http.post<{ratingValue: number}>(`${this.baseUrl}${movieId}/${this.actions.ratings}`, {
      ratingValue: rating
    });
  }

  public saveReview(movieId: number, username: string, title: string, review: string): Observable<{review: string}> {
    return this.http.post<any>(`${this.baseUrl}${movieId}/${this.actions.reviews}`, {
        username,
        title,
        review
    });
  }

}
