import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { AuthService } from "../auth/auth.service";

@Injectable({
    providedIn: 'root'
})
export class UserStateService {
    private _isLoggedIn = false;
    $userStateSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);

    set isLoggedIn(value: boolean) {
        this._isLoggedIn = value;
    }

    get isLoggedIn() {
        return this._isLoggedIn;
    }
    constructor(
        private authService: AuthService
    ) {
        this.$userStateSubject.next(this.authService.getUser() ? true : false);
    }

    unsubscribe() {
        if (this.$userStateSubject && !this.$userStateSubject.closed) {
            this.$userStateSubject.unsubscribe();
        }
    }
}
