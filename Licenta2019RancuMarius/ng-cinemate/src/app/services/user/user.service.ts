import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { UserMovieList } from "src/app/models/movie-list.model";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {

    }

    private baseUrl = "http://localhost:60651/api/user";
    private actions = {
        save: "save",
        favorite: "favorite",
    }

    public addMovieToSaved(movieIdParam: number, userId: string): Observable<{movieId: number}> {
        return this.http.post<{movieId: number}>(`${ this.baseUrl }/${userId}/${this.actions.save}`, {
          movieId: movieIdParam
        });
      }
    
      public addMovieToFavorites(movieIdParam: number, userId: string): Observable<{movieId: number}> {
        return this.http.post<{movieId: number}>(`${ this.baseUrl }/${userId}/${this.actions.favorite}`, {
          movieId: movieIdParam
        });
      }

      public getFavoriteMovies(userId: number): Observable<UserMovieList[]> {
        return this.http.get<UserMovieList[]>(this.baseUrl + '/' + userId + '/' + this.actions.favorite);
      }
    
      public getToSeeMovies(userId: number): Observable<UserMovieList[]> {
        return this.http.get<UserMovieList[]>(this.baseUrl + '/' + userId + '/' + this.actions.save);
      }

      public removeMovieFromFavorite(userId: number, movieId: number): Observable<boolean> {
        return this.http.delete<boolean>(this.baseUrl + '/' + userId + '/' + this.actions.favorite + '/' + movieId);
      }

      public removeMovieFromSaved(userId: number, id: number): Observable<boolean> {
        return this.http.delete<boolean>(this.baseUrl + '/' + userId + '/' + this.actions.save + '/' + id);
      }
}