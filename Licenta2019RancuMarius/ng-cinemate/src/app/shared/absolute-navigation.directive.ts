import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appAbsoluteNavigation]'
})
export class AbsoluteNavigationDirective {
  constructor(
    private renderer2: Renderer2,
    private elementRef: ElementRef
  ) { }
  @HostListener('window:scroll', []) onScroll() {
    if (document.documentElement.scrollTop >= 300) {
      this.renderer2.addClass(this.elementRef.nativeElement, 'sticky-header');
    } else if (document.documentElement.scrollTop < 300) {
         this.renderer2.removeClass(this.elementRef.nativeElement, 'sticky-header');
    }
  }
}
