import { Component, OnInit, Input, ContentChild, TemplateRef, OnChanges, SimpleChanges} from '@angular/core';

export interface AsideDetail {
  entityName: string,
  data: string[] | ItemWithUrl[] | Date | string,
  type: AsideDetailItemType,
  url?: boolean
}

export interface ItemWithUrl {
  itemValue: string,
  url: string
}

export const enum AsideDetailItemType {
  LIST,
  STRING,
  KEYWORD,
  DATE
}

@Component({
  selector: 'app-aside-details',
  templateUrl: './aside-details.component.html',
  styleUrls: ['./aside-details.component.scss']
})
export class AsideDetailsComponent implements OnInit, OnChanges {
  @Input('styleClass') styleClass = '';
  @Input('items') items: AsideDetail[];
  @Input('itemClass') itemClass = '';

  @ContentChild('keyword') keywordTemplate: TemplateRef<any>;
  @ContentChild('list') listTemplate: TemplateRef<any>;
  @ContentChild('string') stringTemplate: TemplateRef<any>;
  @ContentChild('date') dateTemplate: TemplateRef<any>;

  keywordItems: AsideDetail[] = [];
  listItems: AsideDetail[] = [];
  stringItems: AsideDetail[] = [];
  dateItems: AsideDetail[] = [];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['items'] && changes['items'].currentValue) {
      changes['items'].currentValue.forEach( (item: AsideDetail) => {
          if (item.type === AsideDetailItemType.KEYWORD) {
              this.keywordItems.push(item);
          } else if (item.type === AsideDetailItemType.LIST) {
              this.listItems.push(item);
          } else if (item.type === AsideDetailItemType.STRING) {
              this.stringItems.push(item);
          } else if (item.type === AsideDetailItemType.DATE) {
              this.dateItems.push(item);
          }
      });
    }
  }
}
