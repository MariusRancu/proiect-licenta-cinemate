import {Component, Input, OnInit} from '@angular/core';

export enum ChipGenre {
  ACTION = 'Action',
  ADVENTURE = 'Adventure',
  SCIFI = 'Sci-fi',
  DRAMA = 'Drama',
  COMEDY = 'Comedy',
  THRILLER = 'Thriller',
  MYSTERY = 'Mystery',
  CRIME = 'Crime',
  HORROR = 'Horror'
}

@Component({
  selector: 'app-chip',
  templateUrl: './chip.component.html',
  styleUrls: ['./chip.component.scss']
})
export class ChipComponent implements OnInit {
  genres = ChipGenre;
  @Input('genre')
  genre: ChipGenre;
  constructor() { }

  ngOnInit() {
  }

  verifyGenreType(genre: ChipGenre): boolean {
    return this.genre === genre;
  }

}
