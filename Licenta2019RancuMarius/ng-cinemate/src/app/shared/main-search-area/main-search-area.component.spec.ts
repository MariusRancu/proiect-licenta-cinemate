import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSearchAreaComponent } from './main-search-area.component';

describe('MainSearchAreaComponent', () => {
  let component: MainSearchAreaComponent;
  let fixture: ComponentFixture<MainSearchAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSearchAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSearchAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
