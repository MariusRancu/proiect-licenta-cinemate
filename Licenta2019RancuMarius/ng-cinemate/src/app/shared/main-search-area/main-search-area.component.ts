import { Component, OnInit, Output } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search-area.component.html',
  styleUrls: ['./main-search-area.component.scss']
})
export class MainSearchAreaComponent implements OnInit {
  model = {
    value: ''
  };

  @Output() search: EventEmitter = new EventEmitter();

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onKeyDown(event) {
    if (event.key === 'Enter') {
      if (this.model.value) {
          const navigationExtras: NavigationExtras = {
            queryParams: {
              queryString: this.model.value
            }
          };
          console.log("a: ");
          this.router.navigate(['list'], navigationExtras).then( () => {
            this.search.emit(this.model.value);
          });
      }
    }
  }

}
