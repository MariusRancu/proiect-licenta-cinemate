import { Component, OnInit, OnChanges, AfterContentInit, SimpleChanges, Input } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnChanges, AfterContentInit {
  @Input('blocks') blocks: any;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  ngAfterContentInit() {

  }

}
