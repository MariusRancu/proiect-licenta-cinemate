import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
  @Input('disabled') disabled = false;
  @Input('rating')
  set rating(value: number) {
    this._rating = Math.floor(value);
  }
  _rating = 0;
  @Output() onRatingSet: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  onRate(event) {
    this.onRatingSet.next(event);
  }

}
