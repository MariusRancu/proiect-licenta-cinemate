import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MoviesService} from '../../services/movies/movies.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.scss']
})
export class ReviewFormComponent implements OnInit {
  @Input() movieId: number;
  @Output()
  public onModalClose: EventEmitter<any> = new EventEmitter<any>();
  @Output() review: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  height = '60%';

  @Input()
  width = '30%';

  @Input()
  header = '';

  @Input()
  visible = false;

  public form: FormGroup;
  isValid  = true;

  constructor(
    private fb: FormBuilder,
    private movieService: MoviesService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.createFormModel();
  }

  createFormModel() {
    this.form = this.fb.group({
      'title': ['', Validators.required],
      'review': ['', [Validators.required]]
    });
  }

  onHide($event) {
    this.onModalClose.emit();
  }

  onSubmit($event) {
    const username = this.authService.getUserName();
    this.movieService.saveReview(this.movieId, username, this.form.value.title, this.form.value.review).subscribe(
      (value) => {
        this.isValid = true;
        this.visible = false;
        this.review.emit(true);
      },
      (error) => {
        this.isValid = false;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
  }
}
