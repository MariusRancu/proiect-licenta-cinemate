import { NgModule } from '@angular/core';
import * as PrimeNG from 'primeng/primeng';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DataViewModule} from 'primeng/dataview';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  declarations: [
  ],
  providers: [
    PrimeNG.ConfirmationService
  ],
  exports: [
    PrimeNG.CarouselModule,
    PrimeNG.InputTextModule,
    PrimeNG.MenubarModule,
    PrimeNG.ButtonModule,
    PrimeNG.DialogModule,
    PrimeNG.TabViewModule,
    PrimeNG.CardModule,
    PrimeNG.ButtonModule,
    PrimeNG.ConfirmDialogModule,
    PrimeNG.AutoCompleteModule,
    PrimeNG.DropdownModule,
    PrimeNG.RatingModule,
    PrimeNG.LightboxModule,
    PrimeNG.TooltipModule,
    DataViewModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CommonModule
  ]
})
export class SharedLibsModule { }
