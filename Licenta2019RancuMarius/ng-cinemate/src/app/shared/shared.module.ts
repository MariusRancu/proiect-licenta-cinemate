import { NgModule } from '@angular/core';
import { UserAuthenticationComponent } from './user-authentication/user-authentication.component';
import { AbsoluteNavigationDirective } from './absolute-navigation.directive';
import { SharedLibsModule } from './shared-libs/shared-libs.module';
import { UserLoginComponent } from './user-login/user-login.component';
import { CommonModule } from '@angular/common';
import { MainSearchAreaComponent } from './main-search-area/main-search-area.component';
import { AsideDetailsComponent } from './aside-details/aside-details.component';
import { OverviewComponent } from './overview/overview.component';
import { ChipComponent } from './chip/chip.component';
import { SocialNetworkComponent } from './social-network/social-network.component';
import { SocialNetworkItemComponent } from './social-network-item/social-network-item.component';
import { RatingComponent } from './rating/rating.component';
import { ReviewFormComponent } from './review-form/review-form.component';

@NgModule({
  imports: [
    SharedLibsModule,
    CommonModule
  ],
  declarations: [
    UserAuthenticationComponent,
    AbsoluteNavigationDirective,
    UserLoginComponent,
    MainSearchAreaComponent,
    AsideDetailsComponent,
    OverviewComponent,
    ChipComponent,
    SocialNetworkComponent,
    SocialNetworkItemComponent,
    RatingComponent,
    ReviewFormComponent
  ],
  exports: [
    UserAuthenticationComponent,
    UserLoginComponent,
    AbsoluteNavigationDirective,
    SharedLibsModule,
    MainSearchAreaComponent,
    CommonModule,
    AsideDetailsComponent,
    OverviewComponent,
    ChipComponent,
    SocialNetworkComponent,
    RatingComponent,
    ReviewFormComponent
  ]
})
export class SharedModule { }
