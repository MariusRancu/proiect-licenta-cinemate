import {Component, Input, OnInit} from '@angular/core';
import {SocialNetworkItem} from '../social-network/social-network.component';

@Component({
  selector: 'app-social-network-item',
  templateUrl: './social-network-item.component.html',
  styleUrls: ['./social-network-item.component.scss']
})
export class SocialNetworkItemComponent implements OnInit {
  @Input() config: SocialNetworkItem;
  constructor() { }

  ngOnInit() {
  }

}
