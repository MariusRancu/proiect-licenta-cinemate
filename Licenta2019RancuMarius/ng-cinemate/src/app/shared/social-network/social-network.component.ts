import {Component, Input, OnInit} from '@angular/core';

export interface SocialNetworkItem {
  icon: string;
  text: string;
  url: string;
}
@Component({
  selector: 'app-social-network',
  templateUrl: './social-network.component.html',
  styleUrls: ['./social-network.component.scss']
})
export class SocialNetworkComponent implements OnInit {
  @Input() socialNetworkItems: SocialNetworkItem[];
  constructor() { }

  ngOnInit() {
  }

}
