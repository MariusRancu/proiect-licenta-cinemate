import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserRegisterModel } from 'src/app/models/user-register.model';

@Component({
  selector: 'app-user-authentication',
  templateUrl: './user-authentication.component.html',
  styleUrls: ['./user-authentication.component.scss']
})
export class UserAuthenticationComponent implements OnInit {
  public form: FormGroup;

  @Output()
  public onModalClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  height = '70%';

  @Input()
  width = '30%';

  @Input()
  header = '';

  @Input()
  visible = false;

  isRegisterCompleted = true;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.createFormModel();
  }

  createFormModel() {
    this.form = this.fb.group({
      'username': ['', Validators.required],
      'email': ['', [Validators.email, Validators.required]],
      'passwords': this.fb.group({
        'password': ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
        'repeatPassword': ['', Validators.required]
      }, {validator: [this.passwordConfirming]})
    });
  }

  passwordConfirming(c: FormGroup) {
    if (c.controls['repeatPassword'].value === c.controls['password'].value) {
      return null;
    }
    return {missmatch: true};
  }

  onHide($event) {
    this.onModalClose.emit();
  }

  onSubmit() {
    if (this.form.valid) {
      const userModel = new UserRegisterModel(
        this.form.controls['username'].value,
        this.form.controls['email'].value,
        this.form.get('passwords').get('password').value
      );

      this.authService.register(userModel).subscribe(
        (data) => {
          if (data && data.Token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(data));
          }
          this.isRegisterCompleted = true;
          this.onModalClose.emit();
        },
        (error: Error) => {
          this.isRegisterCompleted = false;
        },
        () => {
        }
      );
    }

  }
}
