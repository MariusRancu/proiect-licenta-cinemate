import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserStateService } from 'src/app/services/user/user-state.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  @Output()
  public onModalClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  height = '70%';

  @Input()
  width = '30%';

  @Input()
  header = '';

  @Input()
  visible = false;

  public form: FormGroup;
  isValid  = true;
  loginFailed = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private userStateService: UserStateService
  ) { }

  ngOnInit() {
    this.createFormModel();
  }

  createFormModel() {
    this.form = this.fb.group({
      'email': ['', [Validators.email, Validators.required]],
      'password': ['', Validators.required],
    });
  }

  onHide($event) {
    this.onModalClose.emit();
  }

  onSubmit($event) {
    if (this.form.valid) {
      const email = this.form.controls['email'].value;
      const password = this.form.get('password').value;

      this.authService.login(email, password).subscribe(res => {
        // login successful if there's a jwt token in the response
        if (res && res.Token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(res));//aici se pune ce vine din backend
            this.isValid = true;
            this.onModalClose.emit();
            this.userStateService.$userStateSubject.next(true);
            this.userStateService.isLoggedIn = true;
        }
        this.loginFailed = false;
    }, err => {
      this.isValid = false;
      this.loginFailed = true;
    });
  }
  }
}
