import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import {
  UserComponent,
  ChangePasswordComponent,
  FavoriteMoviesComponent
} from './user';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    UserComponent,
    ChangePasswordComponent,
    FavoriteMoviesComponent
  ]
})
export class UserModule { }
