import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { delay, flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs/internal/observable/of';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit{
  detailsForm: FormGroup;
  changePasswordForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}
  
  username: string;
  email: string;
  redirectMessage: string;

  ngOnInit() {
    this.username = this.authService.getUserName();
    this.email = this.authService.getUserEmail();
    this.createDetailsForm(this.username, this.email);
    this.createChangePasswordForm();
  }

  createDetailsForm(username, email) {
    console.log("username: ", username, email);
    this.detailsForm = this.fb.group({
      username: this.fb.control({value: username, disabled: true}),
      emailAddress: this.fb.control({value: email, disabled: true})
      // firstName: [''],
      // lastName: [''],
      // country: [''],
      // state: ['']
    });
  }

  createChangePasswordForm() {
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
      newPassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
      confirmNewPassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
    });
  }

  passwordConfirming(c: FormGroup) {
    if (this.changePasswordForm.get('newPassword').value === this.changePasswordForm.get('confirmNewPassword').value) {
      return null;
    }
    return {missmatch: true};
  }

  onSubmit(formType) {
    if(formType === 'changePasswordForm' && this.changePasswordForm.valid) {
      this.authService.changePassword(this.changePasswordForm.controls['oldPassword'].value, 
      this.changePasswordForm.controls['newPassword'].value).pipe(
        flatMap( (data) => {
          this.redirectMessage = 'Password changed, you will pe signed out';
          return of(data);
        }), delay(3000)
      ).subscribe(
        (data) => {
          this.authService.logout();
          this.router.navigate(['index'])
        }
      );
    }
  }
}
