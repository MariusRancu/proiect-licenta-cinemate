import { Component, OnInit } from '@angular/core';
import {UserMovieList} from '../../../../models/movie-list.model';
import {MoviesService} from '../../../../services/movies/movies.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../services/auth/auth.service';
import {catchError, flatMap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-favorite-movies',
  templateUrl: './favorite-movies.component.html',
  styleUrls: ['./favorite-movies.component.scss']
})
export class FavoriteMoviesComponent implements OnInit {
  movies: UserMovieList[];
  toSeeMovies: boolean;
  constructor(
    private movieService: MoviesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe( (data) => {
        if (data.toSeeMovies) {
          this.toSeeMovies = data.toSeeMovies;
        }
        this.movies = this.activatedRoute.snapshot.data.movies;
    });
  }

  onRatingSet(event, movieId) {
    this.movieService.saveRating(movieId, event.value).subscribe( (newRating) => {
    });
  }

  onClickMovieDetails(movieId) {
    this.router.navigate(['movies/', movieId]);
  }


  removeMovie(movieId) {
    const userId = this.authService.getUserId();
    this.userService.removeMovieFromSaved(userId, movieId).pipe(
      flatMap( (value: boolean) => {
        if (value) {
          return this.userService.getToSeeMovies(userId);
        }
        return throwError('Movie not deleted!');
      }),
      catchError( (error) => {
        return throwError( 'Error');
      })
    ).subscribe( (data: UserMovieList[]) => {
      this.movies = data;
    }, (error: any) => console.log("error", error));
  }

  removeFavoriteMovie(movieId) {
    const userId = this.authService.getUserId();
    this.userService.removeMovieFromFavorite(userId, movieId).pipe(
      flatMap( (value: boolean) => {
        if (value) {
          return this.userService.getFavoriteMovies(userId);
        }
        return throwError('Movie not deleted!');
      }),
      catchError( (error) => {
        return throwError( 'Error');
      })
    ).subscribe( (data: UserMovieList[]) => {
      this.movies = data;
    }, (error: any) => console.log("error", error));
  }

}
