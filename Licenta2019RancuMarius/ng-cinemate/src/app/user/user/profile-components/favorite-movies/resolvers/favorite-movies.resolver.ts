import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Resolve} from '@angular/router';
import {UserMovieList} from '../../../../../models/movie-list.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class FavoriteMoviesResolver implements Resolve<Observable<UserMovieList[]>> {
  // movies = [
  //   //   {
  //   //     "id": 278,
  //   //     "title": "The Shawshank Redemption",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/9O7gLzmreU0nGkIB6K3BsJbzvNv.jpg",
  //   //     userRating: 5,
  //   //     review: {
  //   //       title: 'Best Marvel movie in my opinion',
  //   //       date: new Date(2017, 3, 2),
  //   //       description: "This is by far one of my favorite movies from the MCU. The introduction of new Characters both good and bad also makes the movie more exciting. giving the characters more of a back story can also help audiences relate more to different characters better, and it connects a bond between the audience and actors or characters. Having seen the movie three times does not bother me here as it is as thrilling and exciting every time I am watching it. In other words, the movie is by far better than previous movies (and I do love everything Marvel), the plotting is splendid (they really do out do themselves in each film, there are no problems watching it more than once."
  //   //     }
  //   //   },
  //   //   {
  //   //     "id": 238,
  //   //     "title": "The Godfather",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/rPdtLWNsZmAtoZl9PK7S2wE3qiS.jpg",
  //   //     userRating: 5
  //   //   },
  //   //   {
  //   //     "id": 629,
  //   //     "title": "The Usual Suspects",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/jgJoRWltoS17nD5MAQ1yK2Ztefw.jpg",
  //   //     userRating: 4
  //   //   },
  //   //   {
  //   //     "id": 424,
  //   //     "title": "Schindler's List",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/yPisjyLweCl1tbgwgtzBCNCBle.jpg",
  //   //     userRating: 3
  //   //   },
  //   //   {
  //   //     "id": 240,
  //   //     "title": "The Godfather: Part II",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/bVq65huQ8vHDd1a4Z37QtuyEvpA.jpg",
  //   //     userRating: 2
  //   //   },
  //   //   {
  //   //     "id": 567,
  //   //     "title": "Rear Window",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/qitnZcLP7C9DLRuPpmvZ7GiEjJN.jpg",
  //   //     userRating: 1
  //   //   },
  //   //   {
  //   //     "id": 346,
  //   //     "title": "Seven Samurai",
  //   //     "rating": 4.27,
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/v6xrz4fr92KY1oNC3HsEvrsvR1n.jpg",
  //   //     userRating: 5
  //   //   },
  //   //   {
  //   //     "id": 289,
  //   //     "title": "Casablanca",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/9UViITBCKeLYWg8Wblbmb9l25mS.jpg",
  //   //     userRating: 5
  //   //   },
  //   //   {
  //   //     "id": 510,
  //   //     "title": "One Flew Over the Cuckoo's Nest",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/2Sns5oMb356JNdBHgBETjIpRYy9.jpg",
  //   //     userRating: 5
  //   //   },
  //   //   {
  //   //     "id": 935,
  //   //     "title": "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb",
  //   //     "posterPath": "http://image.tmdb.org/t/p/w185/tviJ68Wj4glQk3CPMvdvExYHxX.jpg",
  //   //     userRating: 5
  //   //   }
  //   // ];
  movies = [];

  constructor(private authService: AuthService, private userService: UserService) {}

  resolve() {
    const userId = this.authService.getUserId();
    return (this.userService.getFavoriteMovies(userId));
  }
}
