import {Component, OnDestroy, OnInit} from '@angular/core';
import {of, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy{
  categories: string[];
  pageTitle = 'Profile';
  routerEventsSub: Subscription;
  username: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.username = this.authService.getUserName();
    this.routerEventsSub = this.router.events.subscribe( (val) => {
      this.setPageTitle();
    });
  }

  search(event) {
    of (['', 'TV Shows', 'Others']).subscribe((data) => this.categories = data);
  }

  setPageTitle() {
    switch (this.activatedRoute.snapshot.children[0].routeConfig.path) {
      case 'favorite-movies':
        this.pageTitle = 'Favorite Movies';
        break;
      case 'account-details':
        this.pageTitle = 'Profile';
        break;
      case 'to-see-movies':
        this.pageTitle = 'To See Movies';
    }
  }

  ngOnDestroy() {
    if (this.routerEventsSub && !this.routerEventsSub.closed) {
      this.routerEventsSub.unsubscribe();
    }
  }
}
