import {Routes} from '@angular/router';
import {UserComponent} from './user.component';
import {ChangePasswordComponent, FavoriteMoviesComponent} from './profile-components';
import {FavoriteMoviesResolver} from './profile-components/favorite-movies/resolvers/favorite-movies.resolver';
import {ToSeeMoviesResolver} from './profile-components/favorite-movies/resolvers/to-see-movies.resolver';

export const userRoute: Routes = [
  {
    path: 'user',
    children: [
      {
        path: 'profile',
        component: UserComponent,
        children: [
          {
            path: '',
            redirectTo: 'account-details',
            pathMatch: 'full'
          },
          {
            path: 'account-details',
            component: ChangePasswordComponent
          },
          {
            path: 'favorite-movies',
            component: FavoriteMoviesComponent,
            data: {
              toSeeMovies: true
            },
            resolve: {
              movies: FavoriteMoviesResolver
            }
          },
          {
            path: 'to-see-movies',
            component: FavoriteMoviesComponent,
            data: {
              toSeeMovies: false
            },
            resolve: {
              movies: ToSeeMoviesResolver
            }
          }
        ]
      }
    ]
  },
];
